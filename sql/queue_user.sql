/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : runmoney

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-03-21 15:08:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for rm_about
-- ----------------------------
DROP TABLE IF EXISTS `rm_about`;
CREATE TABLE `rm_about` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `typeid` int(10) NOT NULL COMMENT '类型id',
  `title` varchar(50) NOT NULL COMMENT '内容标题',
  `content` varchar(5000) NOT NULL COMMENT '内容',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='关于我们表';

-- ----------------------------
-- Table structure for rm_about_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_about_type`;
CREATE TABLE `rm_about_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL COMMENT '类型名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='关于我们类型表';

-- ----------------------------
-- Table structure for rm_activity
-- ----------------------------
DROP TABLE IF EXISTS `rm_activity`;
CREATE TABLE `rm_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '活动id',
  `target` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '活动目标',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '活动标题',
  `conver` varchar(255) NOT NULL COMMENT '活动封面',
  `starttime` int(10) unsigned NOT NULL COMMENT '活动开始时间',
  `endtime` int(10) NOT NULL COMMENT '活动结束时间',
  `deail` varchar(5000) NOT NULL COMMENT '活动描述',
  `remote_url` varchar(255) NOT NULL COMMENT '活动的远程地址',
  `read_total` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '阅读数',
  `allmoney` decimal(10,0) unsigned NOT NULL DEFAULT '0' COMMENT '活动总金额',
  `number` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '红包个数',
  `create_time` int(10) unsigned NOT NULL COMMENT '活动创建时间',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `businessid` int(10) unsigned NOT NULL COMMENT '商户ID',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `type` tinyint(1) unsigned DEFAULT '0' COMMENT '活动类型：暂无',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8 COMMENT='活动表';

-- ----------------------------
-- Table structure for rm_activity_member
-- ----------------------------
DROP TABLE IF EXISTS `rm_activity_member`;
CREATE TABLE `rm_activity_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `activityid` int(10) unsigned NOT NULL COMMENT '活动ID',
  `create_time` int(10) unsigned NOT NULL COMMENT '报名时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='活动成员表';

-- ----------------------------
-- Table structure for rm_activity_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_activity_record`;
CREATE TABLE `rm_activity_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL COMMENT '用户id',
  `activityid` int(10) unsigned NOT NULL COMMENT '圈子id',
  `amount` decimal(10,2) unsigned NOT NULL COMMENT '红包金额',
  `create_time` int(10) unsigned NOT NULL COMMENT '红包创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否领取，0未领取、1已经领取',
  `receivetime` int(10) unsigned NOT NULL COMMENT '领取红包时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=911 DEFAULT CHARSET=utf8 COMMENT='活动红包记录';

-- ----------------------------
-- Table structure for rm_admin
-- ----------------------------
DROP TABLE IF EXISTS `rm_admin`;
CREATE TABLE `rm_admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `admin_name` varchar(100) DEFAULT NULL COMMENT '后台登录名',
  `admin_password` varchar(32) NOT NULL COMMENT '后台登录密码',
  `realname` varchar(100) DEFAULT NULL COMMENT '后台真实姓名',
  `phone` varchar(20) DEFAULT '' COMMENT '电话',
  `email` varchar(300) DEFAULT '' COMMENT '电子邮件',
  `sex` tinyint(1) unsigned DEFAULT '1' COMMENT '性别：0女；1男；',
  `birthday` date DEFAULT '0000-00-00' COMMENT '生日',
  `head_img` varchar(255) DEFAULT NULL COMMENT '用户头像路径',
  `logincount` int(11) unsigned DEFAULT '0' COMMENT '登录次数',
  `lastuse` datetime DEFAULT NULL COMMENT '最后登录时间',
  `token` varchar(20) DEFAULT NULL COMMENT '登录秘钥',
  `token_time` datetime DEFAULT NULL COMMENT '秘钥生成时间',
  `create` int(10) unsigned DEFAULT NULL COMMENT '创建时间',
  `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_id` int(11) unsigned DEFAULT '0' COMMENT '删除：0否；>0 删除用户id；',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `updpwd` int(10) unsigned DEFAULT '0' COMMENT '修改密码次数 ',
  `updpwd_time` int(10) unsigned DEFAULT '0' COMMENT '修改密码时间（时间戳）',
  `add_id` int(11) unsigned DEFAULT '0' COMMENT '添加人id',
  `role` tinyint(2) unsigned DEFAULT '0' COMMENT '权限：0正常会员；1股东；2财务；3未知；9超权；',
  `status` varchar(30) DEFAULT 'normal' COMMENT '账号状态2018-05-12lixinhe',
  `salt` varchar(30) DEFAULT '' COMMENT '密码盐\r\n2018-05-12\r\nlixinhe',
  `loginfailure` tinyint(1) DEFAULT NULL COMMENT '登录失败次数\r\n2018-05-12\r\nlixinhe',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Table structure for rm_admin_account
-- ----------------------------
DROP TABLE IF EXISTS `rm_admin_account`;
CREATE TABLE `rm_admin_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(16) DEFAULT NULL COMMENT '账号',
  `mobile` varchar(12) DEFAULT NULL COMMENT '手机号',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `status` int(11) DEFAULT '0' COMMENT '状态 （0禁止 1可用）',
  `create_time` int(11) DEFAULT NULL COMMENT '帐号创建时间',
  `administrator` int(1) DEFAULT '0' COMMENT '是否超级管理员，1是 0否',
  `role_id` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT NULL COMMENT '账户最后更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '软删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Table structure for rm_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `rm_admin_log`;
CREATE TABLE `rm_admin_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `content` tinyint(2) unsigned DEFAULT '1' COMMENT '1登录； 2退出； 3扫码登录;',
  `admin_id` int(11) unsigned DEFAULT NULL COMMENT '用户id',
  `admin_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP地址',
  `mc` varchar(50) DEFAULT 'computer' COMMENT 'mobile手机端；computer电脑端；',
  `os` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `lang` varchar(50) DEFAULT NULL COMMENT '语言',
  `browser` varchar(50) DEFAULT 'fail' COMMENT '浏览器：notknow不知道;fail获取失败;Firefox;Chrome;Safari;Opera;',
  `addron` varchar(255) DEFAULT NULL COMMENT '地址（国）',
  `addrtw` varchar(255) DEFAULT NULL COMMENT '地址（省）',
  `addrth` varchar(255) DEFAULT NULL COMMENT '地址（市）',
  `port` varchar(20) DEFAULT NULL COMMENT '访问端口：SERVER_PORT',
  `agent` varchar(300) DEFAULT '' COMMENT '服务引擎：HTTP_USER_AGENT',
  `brand` varchar(10) DEFAULT NULL COMMENT '品牌',
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `url` varchar(1500) DEFAULT '' COMMENT '操作页面2018-05-12lixinhe',
  `contents` text COMMENT '日志内容2018-05-12lixinhe',
  `title` varchar(100) DEFAULT NULL COMMENT '日志标题2018-05-12lixinhe',
  `createtime` int(10) DEFAULT NULL COMMENT '增加时间2018-05-12lixinhe',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=688 DEFAULT CHARSET=utf8 COMMENT='用户登录/退出日志表';

-- ----------------------------
-- Table structure for rm_admin_operation
-- ----------------------------
DROP TABLE IF EXISTS `rm_admin_operation`;
CREATE TABLE `rm_admin_operation` (
  `op_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `state` tinyint(3) unsigned DEFAULT '0' COMMENT '操作是否成功：0成功；1失败；',
  `admin_id` int(11) unsigned DEFAULT NULL COMMENT '用户id',
  `here_id` int(50) unsigned DEFAULT '0' COMMENT '当前id,0为不不关id操作',
  `content` varchar(100) DEFAULT '未知操作' COMMENT '操作内容',
  `remark` text COMMENT '备注',
  `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`op_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='用户操作日志表';

-- ----------------------------
-- Table structure for rm_ad_business
-- ----------------------------
DROP TABLE IF EXISTS `rm_ad_business`;
CREATE TABLE `rm_ad_business` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id,和广告商关联',
  `name` varchar(200) NOT NULL COMMENT '广告商名称',
  `contract` varchar(200) NOT NULL COMMENT '广告商联系人',
  `mobile` varchar(12) NOT NULL COMMENT '广告商联系电话',
  `address` varchar(255) NOT NULL COMMENT '广告商联系地址',
  `address_details` varchar(160) NOT NULL,
  `temp_contract` varchar(60) NOT NULL COMMENT '临时广告商联系人',
  `temp_mobile` varchar(12) NOT NULL COMMENT '临时广告商联系电话',
  `temp_address` varchar(200) NOT NULL COMMENT '临时广告商联系地址',
  `temp_address_details` varchar(160) NOT NULL,
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `delete_id` int(2) NOT NULL DEFAULT '0',
  `add_id` int(10) NOT NULL,
  `remark` varchar(120) NOT NULL COMMENT '备注',
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_ad_img
-- ----------------------------
DROP TABLE IF EXISTS `rm_ad_img`;
CREATE TABLE `rm_ad_img` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `position_id` int(10) NOT NULL,
  `ad_id` int(10) NOT NULL,
  `url` varchar(255) NOT NULL COMMENT '广告腾讯云上传地址',
  `is_main` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否主图: 1.是 0.否',
  `create_time` int(10) NOT NULL,
  `delete_id` int(10) NOT NULL,
  `delete_time` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_ad_position
-- ----------------------------
DROP TABLE IF EXISTS `rm_ad_position`;
CREATE TABLE `rm_ad_position` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_position_no` varchar(22) NOT NULL COMMENT '广告位置编号',
  `ad_position_name` varchar(60) NOT NULL COMMENT '广告位置名称',
  `ad_group` int(10) NOT NULL COMMENT '广告栏目',
  `desc` varchar(50) NOT NULL COMMENT '广告位置描述',
  `create_time` int(10) NOT NULL,
  `delete_id` int(10) NOT NULL DEFAULT '0' COMMENT '状态 0 未删除  >0 删除者id',
  `delete_time` int(11) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `ad_group` (`ad_group`) USING BTREE,
  KEY `deleteid` (`delete_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_ad_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_ad_record`;
CREATE TABLE `rm_ad_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL COMMENT '广告标题',
  `position_id` int(10) NOT NULL COMMENT '广告位id',
  `ad_business_id` int(10) NOT NULL COMMENT '广告商id',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '广告状态: 0.未知  1.待上线   2.上线中',
  `start_time` int(2) NOT NULL COMMENT '上线时间',
  `end_time` int(2) NOT NULL COMMENT '下线时间',
  `target_url` varchar(200) NOT NULL COMMENT '广告跳转URL',
  `is_seller` int(2) NOT NULL DEFAULT '0' COMMENT '是否有销售代表: 0.无  1.有',
  `seller_name` varchar(160) NOT NULL COMMENT '销售代表名称',
  `seller_phone` varchar(12) NOT NULL COMMENT '销售电话',
  `create_time` int(11) NOT NULL,
  `province` varchar(25) NOT NULL COMMENT '省份',
  `city` varchar(30) NOT NULL COMMENT '城市',
  `district` varchar(50) NOT NULL COMMENT '区域',
  `delete_id` int(2) NOT NULL COMMENT '伪删除状态: 0.未删除  >0.删除者id ',
  `delete_time` int(10) NOT NULL COMMENT '删除时间',
  `add_id` int(10) NOT NULL COMMENT '添加广告者id',
  `remark` varchar(255) NOT NULL,
  `pay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付状态 默认0 没有支付，1支付了，2退款',
  PRIMARY KEY (`id`),
  KEY `status` (`status`) USING BTREE,
  KEY `deleteid` (`delete_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_apikey
-- ----------------------------
DROP TABLE IF EXISTS `rm_apikey`;
CREATE TABLE `rm_apikey` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `appid` int(6) NOT NULL,
  `appsecret` varchar(16) NOT NULL,
  `appip` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api表';

-- ----------------------------
-- Table structure for rm_attachment
-- ----------------------------
DROP TABLE IF EXISTS `rm_attachment`;
CREATE TABLE `rm_attachment` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) NOT NULL DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) NOT NULL DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建日期',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `uploadtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `storage` varchar(100) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `scan_id` varchar(50) DEFAULT '',
  `auto_scan` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COMMENT='附件表';

-- ----------------------------
-- Table structure for rm_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `rm_auth_access`;
CREATE TABLE `rm_auth_access` (
  `role_id` mediumint(8) unsigned NOT NULL COMMENT '角色',
  `rule_id` mediumint(8) unsigned NOT NULL COMMENT '规则唯一英文标识,全小写',
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `rule_name` (`rule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限授权表';

-- ----------------------------
-- Table structure for rm_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `rm_auth_group`;
CREATE TABLE `rm_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父组别',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '组名',
  `rules` text NOT NULL COMMENT '规则ID',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='分组表';

-- ----------------------------
-- Table structure for rm_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `rm_auth_group_access`;
CREATE TABLE `rm_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '会员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '级别ID',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限分组表';

-- ----------------------------
-- Table structure for rm_auth_group_rule
-- ----------------------------
DROP TABLE IF EXISTS `rm_auth_group_rule`;
CREATE TABLE `rm_auth_group_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE,
  KEY `weigh` (`weigh`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COMMENT='节点表';

-- ----------------------------
-- Table structure for rm_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `rm_auth_role`;
CREATE TABLE `rm_auth_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '角色名称',
  `pid` smallint(6) DEFAULT NULL COMMENT '父角色ID',
  `status` tinyint(1) unsigned DEFAULT NULL COMMENT '状态',
  `delete_id` int(10) DEFAULT '0' COMMENT '删除 0 未删除 >0 删除者id',
  `delete_time` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `parentId` (`pid`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Table structure for rm_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `rm_auth_rule`;
CREATE TABLE `rm_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `controller` varchar(255) NOT NULL COMMENT '控制器名',
  `action` varchar(255) NOT NULL COMMENT '操作名',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `pid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '父类ID',
  `delete_id` int(4) NOT NULL COMMENT '是否删除 0 未删除 >0 删除人id',
  `power` int(2) NOT NULL COMMENT '是否启用 0 启用 1 未启用',
  `is_admin` int(2) NOT NULL DEFAULT '1' COMMENT '是否管理员 0 前台用户 1 后台管理员',
  `update_time` int(11) NOT NULL COMMENT '账户最后更新时间',
  `step` int(10) NOT NULL COMMENT '菜单规则权重排序',
  `delete_time` int(11) NOT NULL COMMENT '软删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='权限规则表';

-- ----------------------------
-- Table structure for rm_business
-- ----------------------------
DROP TABLE IF EXISTS `rm_business`;
CREATE TABLE `rm_business` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `class` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否地图入驻商户: 0.否 1.是',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '生成方式：0手动生成|1自动生成',
  `logo` varchar(255) NOT NULL COMMENT '商户封面',
  `name` varchar(120) NOT NULL COMMENT '商户名称',
  `tel` varchar(12) NOT NULL COMMENT '商户电话',
  `addra` varchar(255) DEFAULT '' COMMENT '地址：省市区',
  `address` varchar(100) NOT NULL DEFAULT '' COMMENT '详细地址',
  `longitude` decimal(10,6) NOT NULL COMMENT '经度',
  `latitude` decimal(10,6) NOT NULL COMMENT '纬度',
  `total_money` decimal(10,2) NOT NULL COMMENT '总金额',
  `environment_images` varchar(1000) NOT NULL COMMENT '环境图片组',
  `goods_images` varchar(1000) NOT NULL COMMENT '商品图片组',
  `create_time` int(10) NOT NULL,
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `do_day` char(50) DEFAULT '' COMMENT '每周营业时间：,1,2,3,7,',
  `s_do_time` char(50) DEFAULT '' COMMENT '营业时间开始',
  `e_do_time` char(50) DEFAULT '' COMMENT '营业时间结束',
  `hours` varchar(120) NOT NULL COMMENT '营业时间',
  `default` tinyint(1) unsigned DEFAULT '0' COMMENT '默认商户：0否 1是',
  `scope` text NOT NULL COMMENT '经营范围，500字以内',
  `ok` int(1) unsigned NOT NULL DEFAULT '0',
  `ok_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1812 DEFAULT CHARSET=utf8 COMMENT='商户信息表';

-- ----------------------------
-- Table structure for rm_business_img
-- ----------------------------
DROP TABLE IF EXISTS `rm_business_img`;
CREATE TABLE `rm_business_img` (
  `img_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商户图片id',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `business_id` int(11) NOT NULL COMMENT '商户ID',
  `red_id` int(11) NOT NULL COMMENT '商家红包id',
  `comm_id` int(11) unsigned NOT NULL COMMENT '公益换id',
  `saler_id` int(11) unsigned NOT NULL COMMENT '公益换卖家用户id',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '图片类型：0未定义；1封面；2商品；3环境;  4商家红包图片;  5消费红包图片 6.公益换图片 7.公益换评价图片',
  `url` text NOT NULL COMMENT '商户图片路径',
  `size` int(9) unsigned NOT NULL DEFAULT '0' COMMENT '图片大小（字节）',
  `width` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '图片宽（px）',
  `height` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '图片高（px）',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '图片状态：0刚提交；1通过审核；10未通过审核；',
  `status_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '审核人id',
  `status_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '审核时间',
  `status_remark` varchar(200) NOT NULL DEFAULT '' COMMENT '审核备注',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '同步时间',
  `auto_scan` tinyint(1) DEFAULT '1' COMMENT '0未过审	1过审',
  PRIMARY KEY (`img_id`),
  KEY `userid` (`userid`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `red_id` (`red_id`) USING BTREE,
  KEY `businsid` (`business_id`) USING BTREE,
  KEY `deleteid` (`delete_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13569 DEFAULT CHARSET=utf8 COMMENT='商户信息表';

-- ----------------------------
-- Table structure for rm_business_order
-- ----------------------------
DROP TABLE IF EXISTS `rm_business_order`;
CREATE TABLE `rm_business_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL COMMENT '商户ID',
  `ordersn` varchar(20) NOT NULL COMMENT '订单号',
  `money` decimal(10,2) NOT NULL COMMENT '充值金额',
  `status` int(10) NOT NULL COMMENT '充值状态',
  `creat_time` int(10) NOT NULL COMMENT '充值时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户充值记录';

-- ----------------------------
-- Table structure for rm_business_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_business_record`;
CREATE TABLE `rm_business_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `business_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商户ID',
  `issuerid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发红包人id',
  `red_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '红包id',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '金额',
  `actually` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '实际到账金额',
  `ratio` decimal(3,2) unsigned NOT NULL DEFAULT '1.00' COMMENT '到账比=实际到账/红包金额',
  `is_receive` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否领取：0未领取，1已领取，2已退款',
  `is_time` int(10) unsigned DEFAULT '0' COMMENT '领取时间（退款时间）',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `create_day` date DEFAULT NULL COMMENT '日期',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `longitude` decimal(10,6) NOT NULL COMMENT '经度',
  `latitude` decimal(10,6) NOT NULL COMMENT '纬度',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `devices_num` int(11) NOT NULL COMMENT '设备号',
  PRIMARY KEY (`id`),
  KEY `businid` (`business_id`) USING BTREE,
  KEY `red_id` (`red_id`) USING BTREE,
  KEY `is_receive` (`is_receive`) USING BTREE,
  KEY `ok` (`ok`) USING BTREE,
  KEY `create_day` (`create_day`) USING BTREE,
  KEY `isuserid` (`issuerid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=449436 DEFAULT CHARSET=utf8 COMMENT='商户广告记录表';

-- ----------------------------
-- Table structure for rm_bus_voucher
-- ----------------------------
DROP TABLE IF EXISTS `rm_bus_voucher`;
CREATE TABLE `rm_bus_voucher` (
  `vid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '券id',
  `type` tinyint(1) unsigned DEFAULT '1' COMMENT '类型：1满减，2无门槛，3叠加 4遍地红包捆绑, 5精准红包捆绑',
  `cateid` int(10) NOT NULL DEFAULT '1' COMMENT '类别id',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `busid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `vname` varchar(50) NOT NULL DEFAULT '' COMMENT '券名称',
  `vcontent` varchar(100) NOT NULL DEFAULT '' COMMENT '券内容',
  `amount` int(10) unsigned DEFAULT '0' COMMENT '券数量',
  `content` text NOT NULL COMMENT '消费红包宣传语',
  `target_url` varchar(255) NOT NULL COMMENT '消费红包外部链接地址',
  `receive` int(10) unsigned DEFAULT '0' COMMENT '领取数量',
  `condition` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '条件金额',
  `money` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '券金额',
  `day` int(5) unsigned DEFAULT '1' COMMENT '持续时间（单位：天）',
  `step` int(10) unsigned DEFAULT '0' COMMENT '目标步数',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别：0不做限制,1男，2女',
  `age_min` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '年龄最小',
  `age_max` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '年龄最大',
  `education` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '学历：0不限，1小学，2初中，3高中，4中专，5大专，6本科，7硕士，8博士',
  `distance` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '距离（单位：米）：0为距离不限',
  `trading_area` varchar(150) DEFAULT '' COMMENT '商圈',
  `s_time` int(10) unsigned DEFAULT NULL COMMENT '开始时间',
  `e_time` int(10) unsigned DEFAULT NULL COMMENT '结束时间',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` int(10) unsigned DEFAULT NULL COMMENT '创建时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `longitude` decimal(10,6) NOT NULL COMMENT '经度',
  `latitude` decimal(10,6) NOT NULL COMMENT '纬度',
  `lower_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下架id',
  `lower_time` int(10) unsigned NOT NULL COMMENT '下架时间',
  `preview` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否预览: 0.直接发布 1.是预览',
  PRIMARY KEY (`vid`),
  KEY `cat_id` (`cateid`) USING BTREE,
  KEY `user_id` (`userid`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='商家消费红包（优惠券）';

-- ----------------------------
-- Table structure for rm_bus_voucher_cate
-- ----------------------------
DROP TABLE IF EXISTS `rm_bus_voucher_cate`;
CREATE TABLE `rm_bus_voucher_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '消费红包类别id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '类别父级: 0.顶级 >0子级',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '图标地址',
  `name` varchar(30) NOT NULL,
  `delete_id` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除: 0.正常 >0 删除者id',
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_bus_voucher_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_bus_voucher_record`;
CREATE TABLE `rm_bus_voucher_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '领券id',
  `vid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '券id',
  `vno` varchar(50) NOT NULL DEFAULT '' COMMENT '券编号',
  `busid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商户ID',
  `issuerid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发券人id',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `is_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '领取时间（使用时间）',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `create_day` date NOT NULL COMMENT '日期',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `longitude` decimal(10,6) NOT NULL COMMENT '经度',
  `latitude` decimal(10,6) NOT NULL COMMENT '纬度',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `use_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用id',
  `use_time` int(10) unsigned NOT NULL COMMENT '使用时间',
  PRIMARY KEY (`id`),
  KEY `index_vno` (`vno`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=739 DEFAULT CHARSET=utf8 COMMENT='商家消费红包领取记录（优惠券）';

-- ----------------------------
-- Table structure for rm_chat_images
-- ----------------------------
DROP TABLE IF EXISTS `rm_chat_images`;
CREATE TABLE `rm_chat_images` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `group_id` int(10) NOT NULL DEFAULT '0' COMMENT '群组ID',
  `image_site` varchar(50) NOT NULL DEFAULT '' COMMENT '图片位置',
  `images` varchar(255) NOT NULL DEFAULT '' COMMENT '图片参数',
  `create_time` datetime NOT NULL COMMENT '创建日期',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='附件表';

-- ----------------------------
-- Table structure for rm_circle
-- ----------------------------
DROP TABLE IF EXISTS `rm_circle`;
CREATE TABLE `rm_circle` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL COMMENT '创建者用户ID',
  `red_packet` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '红包个数',
  `red_packet_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '红包金额',
  `total_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '圈子总金额',
  `logo` varchar(255) NOT NULL COMMENT '圈子LOGO',
  `name` varchar(32) NOT NULL COMMENT '圈子名称',
  `description` varchar(120) NOT NULL COMMENT '圈子描述',
  `mobile` varchar(11) NOT NULL COMMENT '圈主联系电话',
  `coverid` int(10) NOT NULL COMMENT '圈子封面',
  `targetid` int(10) NOT NULL COMMENT '目标ID',
  `is_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶：1.是 0.否',
  `is_recharge` tinyint(1) unsigned NOT NULL COMMENT '是否充值 0不充值 1充值',
  `is_pwd` tinyint(1) unsigned NOT NULL COMMENT '开启密码验证 0无需密码  1密码验证',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `city` varchar(50) NOT NULL COMMENT '圈子所在市',
  `longitude` double(9,5) NOT NULL COMMENT '经度',
  `latitude` double(9,5) NOT NULL COMMENT '纬度',
  `create_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `tagid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=103999 DEFAULT CHARSET=utf8 COMMENT='圈子表';

-- ----------------------------
-- Table structure for rm_circle_cover
-- ----------------------------
DROP TABLE IF EXISTS `rm_circle_cover`;
CREATE TABLE `rm_circle_cover` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `conver` varchar(255) NOT NULL COMMENT '圈子封面链接',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='圈子封面表';

-- ----------------------------
-- Table structure for rm_circle_member
-- ----------------------------
DROP TABLE IF EXISTS `rm_circle_member`;
CREATE TABLE `rm_circle_member` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL COMMENT '用户ID',
  `circleid` int(10) NOT NULL COMMENT '圈子ID',
  `is_admin` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否管理员 0普通用户 1管理员 2群主',
  `nickname` varchar(32) NOT NULL COMMENT '备注名称',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34015 DEFAULT CHARSET=utf8 COMMENT='圈子成员表';

-- ----------------------------
-- Table structure for rm_circle_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_circle_record`;
CREATE TABLE `rm_circle_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL COMMENT '用户id',
  `circleid` int(10) unsigned NOT NULL COMMENT '圈子id',
  `amount` decimal(10,2) unsigned NOT NULL COMMENT '红包金额',
  `create_time` int(10) unsigned NOT NULL COMMENT '红包创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否领取：0未领取、1已经领取',
  `receivetime` int(10) unsigned NOT NULL COMMENT '领取红包时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74033 DEFAULT CHARSET=utf8 COMMENT='圈子红包记录';

-- ----------------------------
-- Table structure for rm_circle_tags
-- ----------------------------
DROP TABLE IF EXISTS `rm_circle_tags`;
CREATE TABLE `rm_circle_tags` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='圈子标签';

-- ----------------------------
-- Table structure for rm_circle_tags_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_circle_tags_record`;
CREATE TABLE `rm_circle_tags_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `circleid` int(10) NOT NULL,
  `tagid` int(10) NOT NULL,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签使用记录表';

-- ----------------------------
-- Table structure for rm_circle_target
-- ----------------------------
DROP TABLE IF EXISTS `rm_circle_target`;
CREATE TABLE `rm_circle_target` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `target` int(10) NOT NULL COMMENT '目标步数',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='圈子目标表';

-- ----------------------------
-- Table structure for rm_community_cate
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_cate`;
CREATE TABLE `rm_community_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `categroy_name` varchar(200) NOT NULL COMMENT '分类名称',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '分类父级id',
  `sort` int(11) NOT NULL COMMENT '排列顺序',
  `add_id` int(11) NOT NULL COMMENT '添加者用户id',
  `create_time` int(11) unsigned NOT NULL,
  `delete_id` int(1) NOT NULL COMMENT '是否删除: 0.否 >0.用户id',
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_community_comment
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_comment`;
CREATE TABLE `rm_community_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL COMMENT '用户id',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类型: 1.收货评价 2.想要',
  `saler_id` int(11) unsigned NOT NULL COMMENT '卖家用户id',
  `content` varchar(255) NOT NULL COMMENT '评论内容',
  `coment_leavl` int(1) NOT NULL DEFAULT '1' COMMENT '评论等级: 1.好评 2.中评 3.差评',
  `desc_star` int(2) NOT NULL DEFAULT '0' COMMENT '描述相符评星1-5',
  `express_star` int(2) NOT NULL COMMENT '物流服务评星1-5',
  `server_star` int(2) NOT NULL COMMENT '服务态度评星1-5',
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_community_exchange
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_exchange`;
CREATE TABLE `rm_community_exchange` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `cate_id` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '分类id',
  `name` varchar(200) NOT NULL,
  `content` varchar(255) NOT NULL,
  `old_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '原价：0.保密  >0价格',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '宝贝单价价格',
  `credit` int(11) NOT NULL DEFAULT '0',
  `number` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '兑换个数',
  `express_status` int(1) NOT NULL DEFAULT '0' COMMENT '0.待议 1.不包邮 2.包邮',
  `express_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态: 1.正常 2.个人下架 3.系统违规下架',
  `status_time` int(11) NOT NULL COMMENT '状态更换时间',
  `create_time` int(11) NOT NULL,
  `is_top` int(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否置顶: 0.否 1.是',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_id` int(10) NOT NULL DEFAULT '0' COMMENT '删除状态: 0.正常 >0已删除',
  `delete_time` int(11) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_community_express_address
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_express_address`;
CREATE TABLE `rm_community_express_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL COMMENT '用户id',
  `consigner` varchar(200) NOT NULL COMMENT '收件人姓名',
  `mobile` varchar(12) NOT NULL COMMENT '手机号',
  `addr` varchar(255) NOT NULL COMMENT '地址，省市区县',
  `address` varchar(255) NOT NULL COMMENT '详细收获地址',
  `zip_code` int(11) NOT NULL COMMENT '地区编号',
  `alias` varchar(100) NOT NULL COMMENT '地方别名',
  `is_default` int(1) NOT NULL DEFAULT '0' COMMENT '是否默认: 0.否 1.是',
  `delete_id` int(1) NOT NULL,
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_community_order
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_order`;
CREATE TABLE `rm_community_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comm_no` varchar(32) NOT NULL COMMENT '公益宝贝订单编号',
  `order_type` int(1) NOT NULL DEFAULT '1' COMMENT '订单类型: 1.普通订单',
  `shipping_type` int(1) NOT NULL DEFAULT '1' COMMENT '配送方式: 1.物流 2.自提',
  `order_status` int(2) NOT NULL DEFAULT '0' COMMENT '订单类型: 0.待付款 1.待发货 2.待收货 3.待评价(已收货) 4.交易成功 5.已关闭 -1.退款中 -2.已退款',
  `review_status` int(1) NOT NULL DEFAULT '0' COMMENT '评价状态: 0.未评论 1.已评论 2.追加评论',
  `close_order_status` int(1) NOT NULL DEFAULT '0' COMMENT '订单关闭状态: 0.未关闭  1.卖家取消订单 2.买家延时付款自动关闭 3.买家关闭',
  `payment_type` int(1) NOT NULL DEFAULT '1' COMMENT '运费支付方式: 1.钱包余额 2.微信 ',
  `userid` int(11) NOT NULL COMMENT '订单卖家用户id',
  `user_avatar` varchar(255) NOT NULL COMMENT '卖家头像',
  `user_nickname` varchar(160) NOT NULL COMMENT '卖家昵称',
  `buyer_id` int(11) NOT NULL COMMENT '买家用户id',
  `buyer_consigner` varchar(100) NOT NULL COMMENT '买家收货人姓名',
  `buyer_mobile` varchar(15) NOT NULL COMMENT '买家收货人联系方式',
  `buyer_addr` varchar(255) NOT NULL COMMENT '买家收货人地址省市区县',
  `buyer_address` varchar(255) NOT NULL COMMENT '买家收货人详细地址',
  `buyer_zip_code` varchar(140) NOT NULL COMMENT '买家收货人邮编',
  `create_time` int(11) NOT NULL COMMENT '下单时间',
  `remark` varchar(255) NOT NULL COMMENT '卖家发货时备注',
  `apply_refound_time` int(11) unsigned NOT NULL COMMENT '买家申请退款时间',
  `pay_time` int(11) NOT NULL COMMENT '卖家付款时间',
  `consign_time` int(11) NOT NULL COMMENT '卖家发货时间',
  `sign_time` int(11) NOT NULL COMMENT '买家确认收货时间',
  `order_finish_status` int(1) NOT NULL DEFAULT '0' COMMENT '订单完成状态: 0.未完成 1.买家确认收货后 2.买家评价后 3.卖家发货后   ',
  `order_finish_time` int(11) unsigned NOT NULL COMMENT '订单完成时间',
  `express_total` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '订单总邮费',
  `price_total` decimal(10,2) unsigned NOT NULL COMMENT '订单总价格',
  `credit_total` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单总步币',
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_community_order_action
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_order_action`;
CREATE TABLE `rm_community_order_action` (
  `action_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comm_order_id` int(11) NOT NULL,
  `action` varchar(160) NOT NULL,
  `userid` int(11) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `order_status` int(2) NOT NULL,
  `order_status_text` varchar(180) NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_community_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_order_goods`;
CREATE TABLE `rm_community_order_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comm_order_id` int(11) unsigned NOT NULL COMMENT '公益换订单id',
  `comm_id` int(11) unsigned NOT NULL COMMENT '公益换宝贝id',
  `userid` int(11) NOT NULL COMMENT '卖家用户id',
  `buyer_id` int(11) unsigned NOT NULL COMMENT '买家用户id',
  `name` varchar(160) NOT NULL COMMENT '宝贝名称',
  `content` varchar(255) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL COMMENT '宝贝价格',
  `cate_id` int(11) unsigned NOT NULL COMMENT '分类id',
  `cate_name` varchar(255) NOT NULL COMMENT '分类名称',
  `credit` int(11) unsigned NOT NULL COMMENT '宝贝的步币价格',
  `number` int(11) NOT NULL COMMENT '宝贝数量',
  `express_status` int(1) NOT NULL COMMENT '邮寄状态: 0.待议 1.不包邮 2.包邮',
  `express_price` decimal(10,2) NOT NULL,
  `create_time` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_community_order_shipping
-- ----------------------------
DROP TABLE IF EXISTS `rm_community_order_shipping`;
CREATE TABLE `rm_community_order_shipping` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comm_order_id` int(11) NOT NULL,
  `company_name` varchar(160) NOT NULL COMMENT '物流公司名称',
  `company_no` varchar(160) NOT NULL COMMENT '物流公司编号',
  `express_no` varchar(60) NOT NULL COMMENT '物流运单号',
  `userid` int(11) NOT NULL COMMENT '卖家用户id',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_company_account
-- ----------------------------
DROP TABLE IF EXISTS `rm_company_account`;
CREATE TABLE `rm_company_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) NOT NULL COMMENT '开户名称',
  `account_type` int(2) NOT NULL COMMENT '账户类型: 1.对公   2.对私',
  `bank_code` varchar(40) NOT NULL COMMENT '银行编码',
  `bank_card` varchar(150) NOT NULL COMMENT '银行卡卡号',
  `bank_name` varchar(255) NOT NULL COMMENT '开户银行名称',
  `region` varchar(100) NOT NULL COMMENT '银行卡所属地区',
  `subbranch` varchar(150) NOT NULL COMMENT '银行支行名称',
  `power` int(255) NOT NULL COMMENT '是否启用: 1.启用 2.禁用',
  `couplet` varchar(150) NOT NULL COMMENT '银行联行号',
  `delete_id` int(10) NOT NULL DEFAULT '0' COMMENT '状态: 0.正常 >0 删除者id',
  `create_time` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_company_income
-- ----------------------------
DROP TABLE IF EXISTS `rm_company_income`;
CREATE TABLE `rm_company_income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL COMMENT '收入类型: 1.会员充值  2.用户提现扣费',
  `withdraw_way` int(2) NOT NULL DEFAULT '1' COMMENT '提现方式: 1.自动 2.手动',
  `order_no` varchar(36) NOT NULL COMMENT '订单编号',
  `transaction_no` varchar(40) NOT NULL COMMENT '平台提现流水号',
  `bank_account` varchar(80) NOT NULL COMMENT '银行账户名',
  `bank_card` varchar(50) NOT NULL COMMENT '银行卡号',
  `bank_name` varchar(100) NOT NULL DEFAULT '' COMMENT '银行卡名称',
  `amount` decimal(10,2) NOT NULL COMMENT '提现金额',
  `actual_amount` decimal(10,2) NOT NULL COMMENT '实际到账金额',
  `create_day` varchar(10) NOT NULL COMMENT '提现日期',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '收益状态：0.未提现 1.提现成功',
  `create_time` int(10) NOT NULL,
  `remark` varchar(100) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='公司收益(个人会员/商户会员 充值)';

-- ----------------------------
-- Table structure for rm_company_rake
-- ----------------------------
DROP TABLE IF EXISTS `rm_company_rake`;
CREATE TABLE `rm_company_rake` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(10) NOT NULL COMMENT '抽成类别: 1.商家发遍地红包 2.商家发商家红包 3.用户领遍地红包',
  `trade_way` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '交易方式: 1.现金 2.步币',
  `source_id` int(11) NOT NULL COMMENT '来源: 遍地红包id, 商家红包id, 遍地明细红包id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `old_amount` float(11,2) NOT NULL COMMENT '原价',
  `actual_amount` float(11,2) NOT NULL COMMENT '抽取后金额',
  `fee_money` float(11,2) NOT NULL COMMENT '抽取的金额(公司收益)',
  `ratio` float(4,2) NOT NULL COMMENT '抽取比例，小数点',
  `create_time` int(11) NOT NULL,
  `delete_id` int(10) NOT NULL DEFAULT '0',
  `delete_time` int(11) NOT NULL,
  `create_day` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='红包抽成记录表(公司收益)';

-- ----------------------------
-- Table structure for rm_config
-- ----------------------------
DROP TABLE IF EXISTS `rm_config`;
CREATE TABLE `rm_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否平台设置: 0.平台  >0.用户id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) NOT NULL DEFAULT '' COMMENT '分组',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) NOT NULL DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text NOT NULL COMMENT '变量值',
  `content` text NOT NULL COMMENT '变量字典数据',
  `rule` varchar(100) NOT NULL DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) NOT NULL DEFAULT '' COMMENT '扩展属性',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='系统配置';

-- ----------------------------
-- Table structure for rm_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `rm_dynamic`;
CREATE TABLE `rm_dynamic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `dynamic` varchar(1000) NOT NULL COMMENT '动态',
  `images` varchar(2000) NOT NULL COMMENT '图片数组',
  `city` varchar(20) NOT NULL COMMENT '城市',
  `vote` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `country` varchar(50) DEFAULT '' COMMENT '国',
  `province` varchar(50) DEFAULT '' COMMENT '省',
  `county` varchar(50) DEFAULT '' COMMENT '县',
  `village` varchar(50) DEFAULT '' COMMENT '村',
  `position` varchar(50) DEFAULT '' COMMENT '详细地址',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='动态表';

-- ----------------------------
-- Table structure for rm_dynamic_comment
-- ----------------------------
DROP TABLE IF EXISTS `rm_dynamic_comment`;
CREATE TABLE `rm_dynamic_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `dynamicid` int(10) unsigned NOT NULL COMMENT '动态ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论主ID',
  `reply_userid` int(10) NOT NULL DEFAULT '0' COMMENT '回复id',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `content` varchar(500) NOT NULL COMMENT '评论内容',
  `create_time` int(10) unsigned NOT NULL COMMENT '评论时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9424 DEFAULT CHARSET=utf8 COMMENT='动态评论表';

-- ----------------------------
-- Table structure for rm_dynamic_vote
-- ----------------------------
DROP TABLE IF EXISTS `rm_dynamic_vote`;
CREATE TABLE `rm_dynamic_vote` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '点赞id',
  `dynamicid` int(10) unsigned NOT NULL COMMENT '动态ID',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2140 DEFAULT CHARSET=utf8 COMMENT='动态点赞表';

-- ----------------------------
-- Table structure for rm_feedback
-- ----------------------------
DROP TABLE IF EXISTS `rm_feedback`;
CREATE TABLE `rm_feedback` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户di',
  `typeid` int(10) NOT NULL COMMENT '类型ID',
  `mobile` varchar(11) NOT NULL COMMENT '手机号',
  `email` varchar(200) NOT NULL DEFAULT '' COMMENT '邮箱',
  `content` varchar(500) NOT NULL COMMENT '反馈内容',
  `create_time` int(15) unsigned NOT NULL COMMENT '反馈时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=628 DEFAULT CHARSET=utf8 COMMENT='意见反馈表';

-- ----------------------------
-- Table structure for rm_feedback_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_feedback_type`;
CREATE TABLE `rm_feedback_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL COMMENT '反馈类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='意见反馈类型表';

-- ----------------------------
-- Table structure for rm_file
-- ----------------------------
DROP TABLE IF EXISTS `rm_file`;
CREATE TABLE `rm_file` (
  `file_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '协议id',
  `title` varchar(255) DEFAULT '' COMMENT '协议名',
  `content` text COMMENT '协议内容',
  `remote_url` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '协议页面',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='协议文件';

-- ----------------------------
-- Table structure for rm_guide_inviter
-- ----------------------------
DROP TABLE IF EXISTS `rm_guide_inviter`;
CREATE TABLE `rm_guide_inviter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `inviterid` int(11) unsigned NOT NULL COMMENT '邀请人用户id',
  `userid` int(11) unsigned NOT NULL COMMENT '用户id',
  `business_id` int(11) unsigned NOT NULL COMMENT '商铺id',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  `ok` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0.未成功 1.支付成功(邀请成功)',
  `ok_time` int(11) NOT NULL,
  `delete_id` int(11) unsigned NOT NULL,
  `delete_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_guide_userinfo
-- ----------------------------
DROP TABLE IF EXISTS `rm_guide_userinfo`;
CREATE TABLE `rm_guide_userinfo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `is_guide` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否地图入驻商户: 0.否 1.是',
  `guide_vip` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否入驻普通会员: 0.否 1.是',
  `guide_glod_vip` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否入驻金牌会员: 0.否 1.是',
  `create_time` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_income
-- ----------------------------
DROP TABLE IF EXISTS `rm_income`;
CREATE TABLE `rm_income` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `typeid` int(10) unsigned NOT NULL COMMENT '收益类型ID：',
  `amount` decimal(10,2) NOT NULL COMMENT '收益金额',
  `create_time` int(10) unsigned NOT NULL COMMENT '收益时间',
  `source_id` int(10) unsigned DEFAULT NULL COMMENT '来源id（圈子id，用户id，商户id，任务id，详细任务id，红包id）',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103650 DEFAULT CHARSET=utf8 COMMENT='收益表';

-- ----------------------------
-- Table structure for rm_income_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_income_type`;
CREATE TABLE `rm_income_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '收益名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='收益类型表';

-- ----------------------------
-- Table structure for rm_inviter_bonus
-- ----------------------------
DROP TABLE IF EXISTS `rm_inviter_bonus`;
CREATE TABLE `rm_inviter_bonus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型：0未知，1商家红包，10其他',
  `trade_way` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '红包提成的交易类别：1.金额  2.步币',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '获取奖励人id',
  `edid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '被邀请人id',
  `red_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '红包id',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '具体红包记录id',
  `red_map_id` int(11) NOT NULL DEFAULT '0' COMMENT '遍地红包id',
  `red_map_recordid` int(11) NOT NULL DEFAULT '0' COMMENT '遍地红包明细id',
  `bonus` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '个人获得奖励金额',
  `ratio` decimal(3,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '个人提成比例（0.2对应20%）',
  `cbonus` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '公司获得奖励金额',
  `cratio` decimal(3,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '公司提成比例（0.2对应20%）',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22523 DEFAULT CHARSET=utf8 COMMENT='邀请人商家红包分红记录表';

-- ----------------------------
-- Table structure for rm_log_cheat
-- ----------------------------
DROP TABLE IF EXISTS `rm_log_cheat`;
CREATE TABLE `rm_log_cheat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '作弊记录id',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '类型：0未知，1步数，2位置，3红包，10其他',
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `level` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '严重等级（越大越高）',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(1000) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8 COMMENT='用户作弊记录表';

-- ----------------------------
-- Table structure for rm_log_paypw
-- ----------------------------
DROP TABLE IF EXISTS `rm_log_paypw`;
CREATE TABLE `rm_log_paypw` (
  `pwid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '密码日志id',
  `userid` int(11) NOT NULL COMMENT '用户id',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型：0未知，1创建支付密码，2修改支付密码，3验证支付密码，4通过手机号修改支付密码',
  `ip` varchar(200) NOT NULL COMMENT 'IP地址',
  `remark` varchar(120) CHARACTER SET utf8 DEFAULT '' COMMENT '备注',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`pwid`)
) ENGINE=InnoDB AUTO_INCREMENT=4378 DEFAULT CHARSET=utf8mb4 COMMENT='用户支付密码日志表';

-- ----------------------------
-- Table structure for rm_log_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_log_record`;
CREATE TABLE `rm_log_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `os` varchar(18) DEFAULT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='后台操作日志记录';

-- ----------------------------
-- Table structure for rm_log_refund
-- ----------------------------
DROP TABLE IF EXISTS `rm_log_refund`;
CREATE TABLE `rm_log_refund` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '状态： 0系统，0>用户id',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型：1任务退款，2红包退款，3vip回退，4商户会员回退',
  `money` decimal(10,2) unsigned NOT NULL COMMENT '金额',
  `allsum` int(9) DEFAULT NULL COMMENT '数量',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='系统退款记录表';

-- ----------------------------
-- Table structure for rm_log_trade
-- ----------------------------
DROP TABLE IF EXISTS `rm_log_trade`;
CREATE TABLE `rm_log_trade` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_day` date NOT NULL DEFAULT '0000-00-00' COMMENT '创建时间日期',
  `iord` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '交易类型：0未知 1增 2减 3未变化',
  `front` decimal(9,2) NOT NULL DEFAULT '0.00' COMMENT '交易之后用户余额',
  `after` decimal(9,2) NOT NULL DEFAULT '0.00' COMMENT '交易之前用户余额',
  `money` decimal(9,2) NOT NULL COMMENT '交易金额',
  `payment_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '交易方式id（交易方式表）',
  `operation_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '操作类型id（操作类型表）',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `source_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '来源id（圈子id，用户id，商户id，任务id，详细任务id，红包id）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COMMENT='用户交易日志表';

-- ----------------------------
-- Table structure for rm_order
-- ----------------------------
DROP TABLE IF EXISTS `rm_order`;
CREATE TABLE `rm_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `order_type` tinyint(2) unsigned NOT NULL COMMENT '订单类型：0圈子 1用户 2任务 3充值vip 4商户红包 5.退款 6.充值商户会员 7.充值金牌商户会员 8.遍地红包  9.参与奖金赛 10.积分商品购买  11.公益宝贝兑换  12.入驻地图商户  13.办金钻会员  14.办黑钻会员',
  `payment_type` tinyint(2) unsigned NOT NULL COMMENT '支付方式：1钱包；2微信；3支付宝；4小程序；5任务退款;  6云闪付；7苹果内购；8七分钱 9.支付宝',
  `order_no` varchar(32) NOT NULL COMMENT '订单编号',
  `transaction_no` varchar(32) NOT NULL COMMENT '微信支付订单号',
  `channel` varchar(20) NOT NULL COMMENT '七分钱支付渠道 wx，alipay，sevenpay',
  `body` varchar(500) NOT NULL COMMENT '商品描述',
  `total_fee` decimal(10,2) NOT NULL COMMENT '充值金额',
  `apple_pay` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '苹果内购价格',
  `status` tinyint(2) unsigned NOT NULL COMMENT '订单状态 0未支付 1支付成功 2取消订单',
  `create_time` int(10) unsigned NOT NULL COMMENT '充值时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23603 DEFAULT CHARSET=utf8 COMMENT='用户充值订单表';

-- ----------------------------
-- Table structure for rm_order_recharge_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_order_recharge_record`;
CREATE TABLE `rm_order_recharge_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(32) DEFAULT NULL,
  `userid` int(10) unsigned NOT NULL,
  `circleid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '圈子id',
  `taskid` int(10) unsigned DEFAULT '0' COMMENT '任务id',
  `taskno` char(30) NOT NULL DEFAULT '' COMMENT '任务编号（同一人同一时间发布的任务编号相同）',
  `red_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '红包id',
  `vip_no` char(30) DEFAULT '' COMMENT '会员充值编号',
  `cvip_no` char(30) DEFAULT '' COMMENT '商户会员充值编号',
  `gvip_no` char(30) DEFAULT '' COMMENT '金牌商户会员充值编号',
  `red_map_id` int(11) NOT NULL COMMENT '遍地红包id',
  `shop_id` int(11) unsigned NOT NULL COMMENT '步币商城店铺id',
  `shop_order_id` int(11) unsigned NOT NULL COMMENT '步币商城订单id号',
  `comm_order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '公益换订单id',
  `apply_guide_id` int(11) unsigned NOT NULL COMMENT '地图商铺入驻id',
  `g_vip_no` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '金钻会员id',
  `b_vip_no` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '黑钻会员id',
  `racerecord` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '奖金赛参加记录id',
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20171 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_order_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_order_type`;
CREATE TABLE `rm_order_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL COMMENT '订单类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='圈子订单类型表';

-- ----------------------------
-- Table structure for rm_platform_income
-- ----------------------------
DROP TABLE IF EXISTS `rm_platform_income`;
CREATE TABLE `rm_platform_income` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `foreign_id` int(10) NOT NULL DEFAULT '0' COMMENT '外部ID',
  `order_no` varchar(50) NOT NULL DEFAULT '' COMMENT '订单编号',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未知 1:vip 2:business_vip 3:red 4：gold_business_vip',
  `serial_no` varchar(50) DEFAULT '' COMMENT '流水号',
  `area` varchar(255) DEFAULT '' COMMENT '区域',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '金额',
  `arrival` decimal(10,2) DEFAULT '0.00' COMMENT '实际到账金额/实际红包发放金额',
  `deduction` decimal(10,2) DEFAULT '0.00' COMMENT '扣除金额/抽成比',
  `extra` varchar(50) DEFAULT '' COMMENT '扣除原因/红包类型等附属信息',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `payment` int(2) DEFAULT '0' COMMENT '支付类型',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_push_log
-- ----------------------------
DROP TABLE IF EXISTS `rm_push_log`;
CREATE TABLE `rm_push_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `send_type` varchar(10) NOT NULL DEFAULT '',
  `device_type` varchar(20) NOT NULL DEFAULT '',
  `ret` varchar(10) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `out_biz_no` varchar(100) DEFAULT '',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=599 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_push_template
-- ----------------------------
DROP TABLE IF EXISTS `rm_push_template`;
CREATE TABLE `rm_push_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `device_type` varchar(32) NOT NULL COMMENT 'android / ios',
  `name` varchar(32) NOT NULL COMMENT '模板名称',
  `desc` varchar(100) NOT NULL COMMENT '模板描述',
  `template` varchar(500) NOT NULL COMMENT '消息模板',
  `code` int(5) NOT NULL COMMENT '10000/20000/30000/40000….todo….',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '添加时间',
  `update_time` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '最后操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for rm_push_token_log
-- ----------------------------
DROP TABLE IF EXISTS `rm_push_token_log`;
CREATE TABLE `rm_push_token_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `token` varchar(70) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11750 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for rm_redpacket
-- ----------------------------
DROP TABLE IF EXISTS `rm_redpacket`;
CREATE TABLE `rm_redpacket` (
  `red_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '红包条件id',
  `type` tinyint(1) unsigned DEFAULT '1' COMMENT '类型：1个人红包；2商户红包',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `circleid` int(10) NOT NULL COMMENT '当前用户创建圈子id',
  `voucherid` int(11) NOT NULL DEFAULT '0' COMMENT '消费红包id',
  `circle_pwd` varchar(30) NOT NULL COMMENT '圈子密码',
  `businessid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `red_name` varchar(50) DEFAULT '' COMMENT '红包名称',
  `red_content` varchar(100) DEFAULT '' COMMENT '红包内容',
  `target_url` varchar(255) NOT NULL COMMENT '外部链接地址',
  `step` int(10) unsigned DEFAULT '0' COMMENT '目标步数',
  `number` int(10) unsigned DEFAULT '0' COMMENT '红包个数限制',
  `pay_money` decimal(10,2) NOT NULL COMMENT '用户发红包金额，抽成之前金额',
  `ratio` decimal(10,2) NOT NULL COMMENT '商家红包抽成比率',
  `money` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '金额限制',
  `day` int(5) unsigned DEFAULT '1' COMMENT '持续时间（单位：天）',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别：0不做限制,1男，2女',
  `age_min` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '年龄最小',
  `age_max` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '年龄最大',
  `education` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '学历：0不限，1小学，2初中，3高中，4中专，5大专，6本科，7硕士，8博士',
  `distance` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '距离（单位：米）：0为距离不限',
  `trading_area` varchar(150) DEFAULT '' COMMENT '商圈',
  `s_time` int(10) unsigned DEFAULT NULL COMMENT '开始时间',
  `e_time` int(10) unsigned DEFAULT NULL COMMENT '结束时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `country` varchar(50) DEFAULT '' COMMENT '国',
  `province` varchar(50) DEFAULT '' COMMENT '省',
  `county` varchar(50) DEFAULT '' COMMENT '县/区',
  `village` varchar(50) DEFAULT '' COMMENT '村',
  `city` varchar(50) DEFAULT '' COMMENT '市',
  `city_code` varchar(20) DEFAULT '' COMMENT '城市代码',
  `create_time` int(10) unsigned DEFAULT NULL COMMENT '创建时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `rlongitude` decimal(10,6) NOT NULL COMMENT '经度',
  `rlatitude` decimal(10,6) NOT NULL COMMENT '纬度',
  PRIMARY KEY (`red_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2990 DEFAULT CHARSET=utf8 COMMENT='商户/用户红包信息表';

-- ----------------------------
-- Table structure for rm_redpacket_map
-- ----------------------------
DROP TABLE IF EXISTS `rm_redpacket_map`;
CREATE TABLE `rm_redpacket_map` (
  `red_id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '交易方式: 1.金额 2.步币',
  `sex` int(2) NOT NULL DEFAULT '0' COMMENT '性别：0不做限制,1男，2女',
  `age_min` int(3) NOT NULL DEFAULT '0',
  `age_max` int(3) NOT NULL DEFAULT '0',
  `circleid` int(10) NOT NULL,
  `voucherid` int(11) NOT NULL COMMENT '消费红包id',
  `circle_pwd` varchar(30) NOT NULL COMMENT '圈子密码',
  `businessid` int(11) NOT NULL,
  `content` varchar(255) NOT NULL COMMENT '遍地红包描述(200字以内)',
  `number` int(11) NOT NULL DEFAULT '0',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '红包总金额，扣除手续费后的',
  `pay_money` decimal(10,2) NOT NULL,
  `ratio` decimal(11,2) DEFAULT NULL,
  `credit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '步币扣费总额',
  `pay_credit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '步币扣费前总额',
  `receive_num` int(11) NOT NULL COMMENT '红包领取个数',
  `distance` int(11) NOT NULL,
  `target_addr` varchar(255) NOT NULL COMMENT '目标位置地址(省市县)',
  `target_url` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL DEFAULT '0',
  `delete_time` int(11) NOT NULL,
  `longitude` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `latitude` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `ok` tinyint(1) NOT NULL DEFAULT '0',
  `ok_time` int(11) NOT NULL,
  PRIMARY KEY (`red_id`)
) ENGINE=InnoDB AUTO_INCREMENT=862 DEFAULT CHARSET=utf8 COMMENT='遍地红包表';

-- ----------------------------
-- Table structure for rm_redpacket_map_img
-- ----------------------------
DROP TABLE IF EXISTS `rm_redpacket_map_img`;
CREATE TABLE `rm_redpacket_map_img` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `red_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '小程序兑换商品图片',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '图片类别: 1.遍地红包普通图  2.小程序兑换商品图片',
  `url` varchar(255) NOT NULL COMMENT '图片地址',
  `size` int(9) NOT NULL,
  `width` decimal(8,2) NOT NULL,
  `height` decimal(8,2) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '状态: 0. 刚提交 1.通过审核 2.拒绝通过审核',
  `check_admin` int(10) NOT NULL COMMENT '审核管理员id',
  `create_time` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB AUTO_INCREMENT=461 DEFAULT CHARSET=utf8 COMMENT='遍地红包图片表';

-- ----------------------------
-- Table structure for rm_redpacket_map_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_redpacket_map_record`;
CREATE TABLE `rm_redpacket_map_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `red_id` int(11) NOT NULL COMMENT '遍地红包唯一id',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '交易类别: 1.金额  2.步币',
  `business_id` int(11) NOT NULL COMMENT '商户id',
  `from_uid` int(11) NOT NULL COMMENT '发红包人id',
  `before_money` decimal(10,2) NOT NULL COMMENT '发红包之前的金额',
  `money` decimal(10,2) NOT NULL COMMENT '红包金额',
  `ratio` decimal(10,2) NOT NULL DEFAULT '1.00' COMMENT '遍地红包领取抽成比率',
  `credit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '当前用户实际领取的步币',
  `before_credit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '随机分配的步币(含邀请人提成的)',
  `receive_uid` int(11) NOT NULL COMMENT '领取人id',
  `is_receive` tinyint(1) NOT NULL COMMENT '是否领取: 0.未领 1.已领',
  `receive_time` int(10) NOT NULL COMMENT '领取时间',
  `receive_rand` int(11) NOT NULL COMMENT '延时随机时间',
  `receive_day` date NOT NULL COMMENT '领取天',
  `create_time` int(11) NOT NULL,
  `create_day` date NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `longitude` decimal(10,6) NOT NULL,
  `latitude` decimal(10,6) NOT NULL,
  `ok` tinyint(1) NOT NULL COMMENT '是否支付: 0.未支付 1.已支付',
  `ok_time` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL DEFAULT '0',
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=536424 DEFAULT CHARSET=utf8 COMMENT='遍地红包领取记录记录表';

-- ----------------------------
-- Table structure for rm_redpacket_renew
-- ----------------------------
DROP TABLE IF EXISTS `rm_redpacket_renew`;
CREATE TABLE `rm_redpacket_renew` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '红包续费记录明细id',
  `red_type` int(2) NOT NULL COMMENT '红包类型: 1.遍地红包 2.附近',
  `red_id` int(11) unsigned NOT NULL COMMENT '红包id (遍地红包, 附近红包)',
  `userid` int(11) NOT NULL COMMENT '发红包用户id',
  `renew_money` decimal(10,2) NOT NULL COMMENT '续费总金额',
  `actual_money` decimal(10,2) NOT NULL COMMENT '续费扣除手续费金额',
  `ratio` decimal(5,2) NOT NULL DEFAULT '1.00' COMMENT '手续费比例',
  `map_number` int(11) NOT NULL COMMENT '遍地红包续费个数',
  `map_old_number` int(11) NOT NULL COMMENT '遍地红包续费前个数',
  `bred_day` int(10) NOT NULL COMMENT '附近红包续费的任务天数',
  `bred_number` int(10) NOT NULL COMMENT '附近红包续费的单日红包个数',
  `red_data` varchar(255) NOT NULL COMMENT '附近红包原数据',
  `create_time` int(11) NOT NULL,
  `is_pay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否支付: 0.否  1.是',
  `pay_time` int(11) NOT NULL COMMENT '支付时间',
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `type` (`red_type`) USING HASH,
  KEY `userid` (`userid`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='红包续费明细表';

-- ----------------------------
-- Table structure for rm_redpacket_target
-- ----------------------------
DROP TABLE IF EXISTS `rm_redpacket_target`;
CREATE TABLE `rm_redpacket_target` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `distance` int(10) NOT NULL COMMENT '目标步数',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='圈子目标表';

-- ----------------------------
-- Table structure for rm_region_area
-- ----------------------------
DROP TABLE IF EXISTS `rm_region_area`;
CREATE TABLE `rm_region_area` (
  `id` varchar(15) NOT NULL,
  `code_a` varchar(15) NOT NULL,
  `name` varchar(20) NOT NULL,
  `code_c` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`code_a`),
  KEY `FK_Reference_9` (`code_c`) USING BTREE,
  CONSTRAINT `rm_region_area_ibfk_1` FOREIGN KEY (`code_c`) REFERENCES `rm_region_city` (`code_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该实体为行政区域划分等级';

-- ----------------------------
-- Table structure for rm_region_city
-- ----------------------------
DROP TABLE IF EXISTS `rm_region_city`;
CREATE TABLE `rm_region_city` (
  `id` varchar(15) NOT NULL,
  `code_c` varchar(15) NOT NULL,
  `name` varchar(20) NOT NULL,
  `code_p` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`code_c`),
  KEY `FK_Reference_10` (`code_p`) USING BTREE,
  CONSTRAINT `rm_region_city_ibfk_1` FOREIGN KEY (`code_p`) REFERENCES `rm_region_province` (`code_p`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该实体为行政区域划分等级';

-- ----------------------------
-- Table structure for rm_region_province
-- ----------------------------
DROP TABLE IF EXISTS `rm_region_province`;
CREATE TABLE `rm_region_province` (
  `id` varchar(15) NOT NULL,
  `code_p` varchar(15) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`code_p`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该实体为行政区域划分等级';

-- ----------------------------
-- Table structure for rm_scan_log
-- ----------------------------
DROP TABLE IF EXISTS `rm_scan_log`;
CREATE TABLE `rm_scan_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(100) NOT NULL DEFAULT '',
  `method` varchar(100) NOT NULL DEFAULT '',
  `user_id` int(8) NOT NULL,
  `content` text NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=494 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_task
-- ----------------------------
DROP TABLE IF EXISTS `rm_task`;
CREATE TABLE `rm_task` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '专享红包类型: 1.朋友专享 2.长辈专享 3.夫妻专享 4.孩子专享 5.父母专享',
  `trade_way` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '交易方式: 1.现金 2.步币',
  `task_no` char(30) NOT NULL DEFAULT '' COMMENT '任务编号（同一人同一时间发布的任务编号相同）',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `to_userid` int(10) unsigned NOT NULL COMMENT '领取人ID',
  `target_step` int(10) NOT NULL COMMENT '目标步数',
  `task_name` varchar(120) NOT NULL,
  `reward_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '任务总金额',
  `avgmoney` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '每个奖励金额',
  `credit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '步币总个数',
  `avgcredit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '每人奖励步币',
  `task_days` int(10) NOT NULL COMMENT '任务天数',
  `task_rule` varchar(255) NOT NULL COMMENT '任务规则',
  `task_ios_desc` varchar(500) NOT NULL COMMENT '任务说明',
  `task_desc` varchar(500) NOT NULL COMMENT '任务说明',
  `start_time` int(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(10) unsigned NOT NULL COMMENT '任务结束时间',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2209 DEFAULT CHARSET=utf8 COMMENT='任务表';

-- ----------------------------
-- Table structure for rm_task_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_task_record`;
CREATE TABLE `rm_task_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_no` char(30) NOT NULL DEFAULT '' COMMENT '任务编号（同一人同一时间发布的任务编号相同）',
  `taskid` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned NOT NULL COMMENT '领取人用户ID',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '专享红包类型：1.朋友专享 2.长辈专享 3.夫妻专享 4.孩子专享 5.父母专享',
  `trade_way` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '交易方式: 1.现金 2.步币',
  `amount` decimal(10,2) unsigned NOT NULL,
  `credits` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '每人每日完成任务领取的步币',
  `activity_start_time` int(10) unsigned NOT NULL COMMENT '活动开始时间',
  `activity_end_time` int(10) unsigned NOT NULL COMMENT '活动结束时间',
  `activity_day` date DEFAULT NULL COMMENT '日期',
  `is_receive` tinyint(1) unsigned NOT NULL COMMENT '是否领取 0未领取 1已领取任务 2已领取奖励',
  `receive_time` int(10) unsigned NOT NULL COMMENT '领取时间',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `is_refund` tinyint(1) unsigned DEFAULT '0' COMMENT '是否退款：0否，1已退',
  `refund_time` int(11) unsigned DEFAULT '0' COMMENT '退款时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8879 DEFAULT CHARSET=utf8 COMMENT='任务详情表';

-- ----------------------------
-- Table structure for rm_trade_operation
-- ----------------------------
DROP TABLE IF EXISTS `rm_trade_operation`;
CREATE TABLE `rm_trade_operation` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '操作类型id',
  `name` varchar(50) NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='操作类型表';

-- ----------------------------
-- Table structure for rm_trade_payment
-- ----------------------------
DROP TABLE IF EXISTS `rm_trade_payment`;
CREATE TABLE `rm_trade_payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '操作类型id',
  `name` varchar(50) NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='交易方式表';

-- ----------------------------
-- Table structure for rm_user
-- ----------------------------
DROP TABLE IF EXISTS `rm_user`;
CREATE TABLE `rm_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` bigint(11) NOT NULL DEFAULT '0' COMMENT '用户编号：替换id显示',
  `balance` decimal(10,3) unsigned NOT NULL COMMENT '总金额',
  `wx_unionid` varchar(100) NOT NULL COMMENT 'unionid（微信对小程序和公众号的唯一标识）',
  `wx_openid` varchar(100) NOT NULL COMMENT 'openid',
  `xcx_openid` varchar(100) NOT NULL DEFAULT '' COMMENT '小程序openid',
  `qq_unionid` varchar(100) NOT NULL COMMENT 'qq的unionid',
  `qq_openid` varchar(100) NOT NULL COMMENT 'qq ios 的openid',
  `qq_ad_openid` varchar(100) NOT NULL COMMENT 'qq 安卓的openid',
  `mobile` varchar(11) NOT NULL COMMENT '手机号',
  `password` varchar(40) NOT NULL COMMENT '密码',
  `avatar` varchar(1000) NOT NULL DEFAULT '' COMMENT '头像地址',
  `nickname` varchar(20) NOT NULL COMMENT '用户昵称',
  `levelid` int(10) NOT NULL COMMENT '段位ID',
  `sex` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '性别： 0未知；1男；2女；',
  `education` tinyint(2) unsigned NOT NULL COMMENT '学历：0不限，1小学，2初中，3高中，4中专，5大专，6本科，7硕士，8博士',
  `birthyear` smallint(5) unsigned NOT NULL COMMENT '生日年份',
  `birthmonth` tinyint(3) unsigned NOT NULL COMMENT '生日月份',
  `birthday` tinyint(3) unsigned NOT NULL COMMENT '生日',
  `height` int(5) unsigned DEFAULT '0' COMMENT '升高（cm）',
  `weight` decimal(5,2) unsigned DEFAULT '0.00' COMMENT '体重（kg）',
  `type` tinyint(2) unsigned NOT NULL COMMENT '用户类型',
  `province` varchar(30) NOT NULL COMMENT '所在省',
  `city` varchar(30) NOT NULL COMMENT '所在市',
  `area` varchar(255) DEFAULT '' COMMENT '区域',
  `target_step` int(10) unsigned NOT NULL DEFAULT '10000' COMMENT '目标步数',
  `credit` int(10) unsigned NOT NULL COMMENT '步币',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1禁用',
  `is_perfect` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否完善资料 0未完善 1已完善',
  `create_time` int(10) unsigned NOT NULL COMMENT '注册时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned DEFAULT NULL COMMENT '删除时间',
  `logintimes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `h_long` decimal(10,6) DEFAULT '0.000000' COMMENT '经度-家',
  `h_lat` decimal(10,6) DEFAULT '0.000000' COMMENT '纬度-家',
  `h_updatetime` int(10) unsigned DEFAULT '0' COMMENT '更新时间',
  `c_long` decimal(10,6) DEFAULT '0.000000' COMMENT '经度-公司',
  `c_lat` decimal(10,6) DEFAULT '0.000000' COMMENT '纬度-公司',
  `c_updatetime` int(10) unsigned DEFAULT '0' COMMENT '更新时间',
  `n_long` decimal(10,6) DEFAULT '0.000000' COMMENT '经度-实时',
  `n_lat` decimal(10,6) DEFAULT '0.000000' COMMENT '纬度-实时',
  `n_updatetime` int(10) unsigned DEFAULT '0' COMMENT '更新时间',
  `vip` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '会员状态：0非会员，1会员',
  `cusvip` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '商户会员状态：0非会员，1会员',
  `gvip` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '金牌商户会员状态：0非会员，1会员',
  `xcx_qr_img` varchar(300) DEFAULT '' COMMENT '小程序二维码图片',
  `xcx_qr_time` int(10) unsigned DEFAULT '0' COMMENT '生成二维码时间',
  `inviter_id` int(10) unsigned DEFAULT '0' COMMENT '引荐人id（推荐我的人）',
  `inviter_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '返现给邀请人的金额',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组别ID',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `successions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '连续登录天数',
  `maxsuccessions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '最大连续登录天数',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `prevtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录时间',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `jointime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
  `joinip` varchar(50) NOT NULL DEFAULT '' COMMENT '加入IP',
  `loginip` varchar(50) NOT NULL DEFAULT '' COMMENT '登录IP',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'Token',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `isreal` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '实名认证：0否，1是',
  `paypw` char(50) NOT NULL COMMENT '支付密码',
  `pwsalt` varchar(10) NOT NULL DEFAULT '' COMMENT '密码盐',
  `pwtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '支付密码更新时间',
  `devices_num` int(11) NOT NULL COMMENT '设备号',
  PRIMARY KEY (`id`),
  KEY `index_wx_unionid` (`wx_unionid`(32)) USING BTREE COMMENT '微信索引',
  KEY `index_qq_unionid` (`qq_unionid`(40)) USING BTREE COMMENT 'qq索引',
  KEY `index_no` (`no`) USING BTREE COMMENT '编号索引',
  KEY `index_mobile` (`mobile`) USING BTREE COMMENT '手机索引',
  KEY `nlan` (`h_long`) USING BTREE,
  KEY `nlen` (`h_lat`) USING BTREE,
  KEY `deleteid` (`delete_id`) USING BTREE,
  KEY `n_long` (`n_long`) USING BTREE,
  KEY `n_lat` (`n_lat`) USING BTREE,
  KEY `inviter_id` (`inviter_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3530746 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Table structure for rm_user_agent
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_agent`;
CREATE TABLE `rm_user_agent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `no` varchar(60) NOT NULL,
  `userid` int(11) NOT NULL,
  `type` int(5) unsigned NOT NULL DEFAULT '0' COMMENT '1.地级市代理商  2.区县代理商  3.跨区代理商',
  `speed_money` decimal(10,2) NOT NULL,
  `actual_money` decimal(10,2) NOT NULL,
  `day` int(11) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `create_day` date NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `ok` int(1) NOT NULL,
  `ok_time` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `rback` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='合作代理商办理表';

-- ----------------------------
-- Table structure for rm_user_authentication
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_authentication`;
CREATE TABLE `rm_user_authentication` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `realname` varchar(32) NOT NULL COMMENT '真实姓名',
  `idcard` varchar(18) NOT NULL COMMENT '身份证号码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '认证状态 0认证中 1认证通过 2认证未通过',
  `remark` varchar(120) DEFAULT NULL,
  `create_time` int(10) NOT NULL COMMENT '认证时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=764 DEFAULT CHARSET=utf8 COMMENT='用户身份验证';

-- ----------------------------
-- Table structure for rm_user_bank
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_bank`;
CREATE TABLE `rm_user_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '用户id',
  `account_name` varchar(50) NOT NULL COMMENT '银行卡户主名',
  `bank_name` varchar(50) NOT NULL COMMENT '银行卡名称',
  `bank_code` varchar(50) NOT NULL COMMENT '银行编码',
  `bank_card` varchar(50) NOT NULL COMMENT '银行卡卡号',
  `bank_phone` varchar(20) NOT NULL COMMENT '银行卡绑定手机号',
  `region` varchar(200) NOT NULL COMMENT '开户地区',
  `couplet` varchar(20) DEFAULT '' COMMENT '联行号',
  `subbranch` varchar(200) DEFAULT '' COMMENT '开户支行',
  `is_bind` int(2) unsigned NOT NULL COMMENT '是否绑定 1.绑定 2.解绑',
  `bind_time` int(11) NOT NULL COMMENT '绑定时间',
  `unbind_time` int(11) NOT NULL COMMENT '解绑时间',
  `iscreditcard` tinyint(1) unsigned DEFAULT '1' COMMENT '信用卡：1借记卡 2信用卡',
  `cyear` int(4) DEFAULT NULL COMMENT '信用卡到期年份',
  `cday` int(2) DEFAULT NULL COMMENT '信用卡到期月份',
  `ccode` int(4) DEFAULT NULL COMMENT '信用卡末三码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=832 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_bank_card
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_bank_card`;
CREATE TABLE `rm_user_bank_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户提现账号id',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `typeid` int(10) unsigned NOT NULL COMMENT '提现类型： 1微信 2支付宝 3银行卡',
  `realname` varchar(20) NOT NULL COMMENT '真实姓名',
  `bank_card` varchar(120) NOT NULL COMMENT '银行卡',
  `opening_bank` varchar(255) NOT NULL COMMENT '开户行',
  `subbranch` varchar(255) NOT NULL COMMENT '开户支行',
  `region` varchar(255) NOT NULL COMMENT '开户地区',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='用户银行卡表';

-- ----------------------------
-- Table structure for rm_user_bank_img
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_bank_img`;
CREATE TABLE `rm_user_bank_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '银行卡图标id',
  `bank_name` varchar(50) NOT NULL COMMENT '银行卡名称',
  `bank_code` varchar(50) NOT NULL COMMENT '银行编码',
  `img_url` varchar(500) NOT NULL COMMENT '图片地址',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='银行卡图标';

-- ----------------------------
-- Table structure for rm_user_chat_recover_log
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_chat_recover_log`;
CREATE TABLE `rm_user_chat_recover_log` (
  `user_id` bigint(11) unsigned NOT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for rm_user_chat_token
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_chat_token`;
CREATE TABLE `rm_user_chat_token` (
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT '用户id',
  `token` varchar(128) NOT NULL DEFAULT '' COMMENT 'token',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_code
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_code`;
CREATE TABLE `rm_user_code` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(11) NOT NULL COMMENT '手机号',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `sendtime` int(10) NOT NULL COMMENT '发送时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=utf8 COMMENT='用户验证码表';

-- ----------------------------
-- Table structure for rm_user_collection
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_collection`;
CREATE TABLE `rm_user_collection` (
  `collid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '收藏id',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '类型：1商家，2圈子，3动态, 4公益换',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '内容',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '同步时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `remark` varchar(300) NOT NULL DEFAULT '' COMMENT '备注',
  `businessid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户id',
  `circleid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '圈子id',
  `dynamicid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '动态id',
  `comm_id` int(11) unsigned NOT NULL COMMENT '公益换宝贝id',
  PRIMARY KEY (`collid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='用户收藏表';

-- ----------------------------
-- Table structure for rm_user_credit
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_credit`;
CREATE TABLE `rm_user_credit` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL COMMENT '用户ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型：1邀请用户奖励 2新人签到21天奖励 3奖金赛奖励 4.开机奖励 5.5000步奖励 6.10000步奖励 7.出厂定制邀请 8.步币兑换消耗 9.发布遍地红包 10.领遍地红包  11.推荐新人奖(遍地 ) 12.发布专享红包 13.领专享红包 14.分享奖励 15.公益宝贝兑换 16.公益换退还 17.公益换收入',
  `source` int(120) unsigned NOT NULL COMMENT '记录名称',
  `credit` int(10) NOT NULL COMMENT '步币额',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='积分记录表';

-- ----------------------------
-- Table structure for rm_user_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_evaluate`;
CREATE TABLE `rm_user_evaluate` (
  `eid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `grade` tinyint(1) unsigned DEFAULT '0' COMMENT '等级评论：0顶级，1二级',
  `father` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '表内上级id',
  `style` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '评价类型：1评论，2点赞',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '记录类型：1商家，2圈子，3动态，4.遍地红包 5每日步数记录',
  `score` decimal(3,1) NOT NULL DEFAULT '0.0' COMMENT '评分（1-10）',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '内容',
  `businessid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户id',
  `circleid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '圈子id',
  `redid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '红包id',
  `red_map_id` int(11) NOT NULL COMMENT '遍地红包id',
  `user_step_rid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户每日步数记录id',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '同步时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `remark` varchar(300) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`eid`),
  KEY `useri_index` (`userid`) USING BTREE,
  KEY `father_index` (`father`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=utf8 COMMENT='用户收藏表';

-- ----------------------------
-- Table structure for rm_user_first
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_first`;
CREATE TABLE `rm_user_first` (
  `fid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '第一个红包id',
  `userid` int(11) NOT NULL,
  `money` decimal(9,2) NOT NULL,
  `credit` int(10) unsigned NOT NULL COMMENT '步币数量',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '领取时间',
  `create_day` date NOT NULL DEFAULT '0000-00-00' COMMENT '领取时间日期',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=1893 DEFAULT CHARSET=utf8 COMMENT='新人注册红包领取记录（系统送的红包）';

-- ----------------------------
-- Table structure for rm_user_follow
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_follow`;
CREATE TABLE `rm_user_follow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `followid` int(10) unsigned NOT NULL COMMENT '关注ID',
  `create_time` int(10) unsigned NOT NULL COMMENT '关注时间',
  `remark_name` varchar(50) DEFAULT '' COMMENT '备注姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12635 DEFAULT CHARSET=utf8 COMMENT='用户关注表';

-- ----------------------------
-- Table structure for rm_user_friends
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_friends`;
CREATE TABLE `rm_user_friends` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `userid` int(10) NOT NULL,
  `friendid` int(10) NOT NULL COMMENT '好友ID',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户好友表';

-- ----------------------------
-- Table structure for rm_user_group
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_group`;
CREATE TABLE `rm_user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '' COMMENT '组名',
  `rules` text COMMENT '权限节点',
  `createtime` int(10) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='会员组表';

-- ----------------------------
-- Table structure for rm_user_growing_level
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_growing_level`;
CREATE TABLE `rm_user_growing_level` (
  `uid` int(11) NOT NULL COMMENT 'userid',
  `user_growing_level` tinyint(2) NOT NULL DEFAULT '0' COMMENT '等级id',
  `name` varchar(16) DEFAULT NULL COMMENT '等级name',
  `pre_recomand_gvip_amount` int(10) DEFAULT '0' COMMENT '上次推荐的金牌会员数量',
  `bonus_given` tinyint(1) DEFAULT '0' COMMENT '金牌推荐奖支付状态默认0 没有支付',
  `bonus` decimal(13,3) DEFAULT NULL COMMENT '上次记录的奖金数',
  `create_time` int(10) DEFAULT NULL,
  `update_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_grow_level
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_grow_level`;
CREATE TABLE `rm_user_grow_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `number` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上个阶段等级',
  `grow_level` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '当前等级: 0.初级用户 1.中级用户 2.高级用户 3.个人会员',
  `create_time` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_inviter
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_inviter`;
CREATE TABLE `rm_user_inviter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inviterid` int(10) unsigned NOT NULL COMMENT '邀请人id',
  `userid` int(10) unsigned NOT NULL COMMENT '被邀请人id',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '邀请码',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127219 DEFAULT CHARSET=utf8 COMMENT='邀请好友表';

-- ----------------------------
-- Table structure for rm_user_inviter_fails
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_inviter_fails`;
CREATE TABLE `rm_user_inviter_fails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `is_success` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否指定成功: 1.成功 0.失败',
  `userid` int(11) NOT NULL COMMENT '用户id',
  `inviter` int(11) NOT NULL DEFAULT '0' COMMENT '指定的邀请人id',
  `old_inviter` int(11) NOT NULL COMMENT '用户原邀请人id',
  `product` varchar(50) NOT NULL COMMENT '手机制造商',
  `hardware` varchar(40) NOT NULL COMMENT '硬件名称',
  `model` varchar(10) NOT NULL COMMENT '版本',
  `cpu_abi` varchar(50) NOT NULL COMMENT 'CPU指令集',
  `error_reason` varchar(100) NOT NULL COMMENT '指定失败原因',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='手机出厂指定邀请人记录表';

-- ----------------------------
-- Table structure for rm_user_level
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_level`;
CREATE TABLE `rm_user_level` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `level` varchar(120) NOT NULL COMMENT '段位名称',
  `target` int(10) unsigned NOT NULL COMMENT '达标',
  `end` int(10) unsigned NOT NULL COMMENT '上限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='段位表';

-- ----------------------------
-- Table structure for rm_user_level_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_level_record`;
CREATE TABLE `rm_user_level_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `levelid` int(10) NOT NULL,
  `create_time` int(10) NOT NULL,
  `see` tinyint(1) unsigned DEFAULT '0' COMMENT '是否查看: 0否，1是',
  `see_time` int(10) unsigned DEFAULT '0' COMMENT '查看时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4380 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_location
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_location`;
CREATE TABLE `rm_user_location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '位置记录id',
  `side` tinyint(2) unsigned NOT NULL DEFAULT '3' COMMENT '用户端：1ios，2android，3小程序，4其他',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `longitude` decimal(10,6) NOT NULL COMMENT '经度',
  `latitude` decimal(10,6) NOT NULL COMMENT '纬度',
  `ip` varchar(50) NOT NULL COMMENT 'IP',
  `create` datetime DEFAULT NULL COMMENT '日期格式',
  `create_time` int(11) unsigned NOT NULL COMMENT '登陆时间',
  `delete_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(11) unsigned DEFAULT NULL COMMENT '软删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=198720 DEFAULT CHARSET=utf8 COMMENT='用户位置记录';

-- ----------------------------
-- Table structure for rm_user_log
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_log`;
CREATE TABLE `rm_user_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `type` tinyint(1) unsigned DEFAULT '1' COMMENT '记录类型：1手机号码登录； 2第三方登录；10退出;',
  `side` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户端：0未知，1ios，2android，3小程序，4web',
  `userid` int(11) unsigned DEFAULT NULL COMMENT '用户id',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `pattern` tinyint(1) unsigned DEFAULT '1' COMMENT '登录方式：1手机密码，2微信，3qq，4手机验证码',
  `remark` varchar(150) DEFAULT '' COMMENT '登录的附加数据：手机号，openid',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP地址',
  `mc` varchar(50) DEFAULT 'computer' COMMENT 'mobile手机端；computer电脑端；',
  `os` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `lang` varchar(50) DEFAULT NULL COMMENT '语言',
  `browser` varchar(50) DEFAULT 'fail' COMMENT '浏览器：notknow不知道;fail获取失败;Firefox;Chrome;Safari;Opera;',
  `addron` varchar(150) DEFAULT NULL COMMENT '地址（国）',
  `addrtw` varchar(150) DEFAULT NULL COMMENT '地址（省）',
  `addrth` varchar(150) DEFAULT NULL COMMENT '地址（市）',
  `port` varchar(20) DEFAULT NULL COMMENT '访问端口：SERVER_PORT',
  `agent` varchar(500) DEFAULT '' COMMENT '服务引擎：HTTP_USER_AGENT',
  `brand` varchar(10) DEFAULT NULL COMMENT '品牌',
  `devices_num` int(11) NOT NULL COMMENT '设备号',
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48245 DEFAULT CHARSET=utf8 COMMENT='用户登录/退出日志表';

-- ----------------------------
-- Table structure for rm_user_map_guide
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_map_guide`;
CREATE TABLE `rm_user_map_guide` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `map_business_id` int(11) NOT NULL,
  `vip_record_id` int(11) NOT NULL DEFAULT '0' COMMENT '0.添加地图店铺    >0.会员办理记录id',
  `type` int(2) unsigned NOT NULL DEFAULT '1' COMMENT '登录商家类型: 1.添加地图商铺  2.办理商家会员 3.办理金牌会员 4.办理金钻会员 5.办理黑钻会员',
  `create_time` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `ok` int(1) NOT NULL,
  `ok_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_messages
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_messages`;
CREATE TABLE `rm_user_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dynamicid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '动态id',
  `from_userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送方用户id',
  `to_userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '接收方用户ID',
  `typeid` int(10) unsigned NOT NULL COMMENT '消息类型id：1|2|3|4',
  `title` varchar(120) DEFAULT '' COMMENT '消息标题',
  `content` text NOT NULL COMMENT '消息内容',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `see` tinyint(3) unsigned DEFAULT '0' COMMENT '是否已读：0否，1是',
  `see_time` int(10) unsigned DEFAULT '0' COMMENT '标记已读时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11204 DEFAULT CHARSET=utf8 COMMENT='消息表';

-- ----------------------------
-- Table structure for rm_user_messages_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_messages_type`;
CREATE TABLE `rm_user_messages_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `typename` varchar(50) NOT NULL COMMENT '消息类型表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='消息类型表';

-- ----------------------------
-- Table structure for rm_user_multi
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_multi`;
CREATE TABLE `rm_user_multi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `uuid` varchar(100) NOT NULL,
  `appid` varchar(100) NOT NULL,
  `mtoken` varchar(300) NOT NULL,
  `is_login` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_push_token
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_push_token`;
CREATE TABLE `rm_user_push_token` (
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `token` varchar(100) NOT NULL COMMENT 'push_token',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `update_time` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '最后变更时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for rm_user_rake_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_rake_record`;
CREATE TABLE `rm_user_rake_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `no` varchar(30) NOT NULL,
  `type` int(11) unsigned NOT NULL COMMENT '分成类型: 1.登录商家(办商家会员/入驻地图)  2.办金牌会员  3.办金钻会员  4.办黑钻会员  5.现金红包分成',
  `action_name` varchar(255) NOT NULL,
  `userid` int(11) unsigned NOT NULL,
  `money_total` decimal(10,2) unsigned NOT NULL COMMENT '分账总金额',
  `one_uid` int(11) unsigned NOT NULL,
  `two_uid` int(11) unsigned NOT NULL,
  `one_levels_money` decimal(10,2) unsigned NOT NULL,
  `two_levels_money` decimal(10,2) unsigned NOT NULL,
  `company_money` decimal(10,2) unsigned NOT NULL,
  `source` varchar(60) NOT NULL,
  `desc` varchar(255) NOT NULL COMMENT '对当前用户的上两级是否为金牌会员的描述',
  `remark` varchar(255) NOT NULL,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_record`;
CREATE TABLE `rm_user_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '位置记录id',
  `type` tinyint(255) unsigned DEFAULT '1' COMMENT '记录类型：1登录，2退出',
  `side` tinyint(2) unsigned NOT NULL DEFAULT '3' COMMENT '用户端：1ios，2android，3小程序，4其他',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `longitude` decimal(9,5) NOT NULL COMMENT '经度',
  `latitude` decimal(9,5) NOT NULL COMMENT '纬度',
  `ip` varchar(50) NOT NULL COMMENT 'IP',
  `create` datetime DEFAULT NULL COMMENT '日期格式',
  `create_time` int(11) unsigned NOT NULL COMMENT '登陆时间',
  `delete_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(11) unsigned DEFAULT NULL COMMENT '软删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='用户登陆记录';

-- ----------------------------
-- Table structure for rm_user_rule
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_rule`;
CREATE TABLE `rm_user_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `title` varchar(50) DEFAULT '' COMMENT '标题',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) DEFAULT NULL COMMENT '是否菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) DEFAULT '0' COMMENT '权重',
  `status` enum('normal','hidden') DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='会员规则表';

-- ----------------------------
-- Table structure for rm_user_share
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_share`;
CREATE TABLE `rm_user_share` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分享id',
  `userid` int(11) unsigned NOT NULL COMMENT '用户id',
  `type` int(11) unsigned NOT NULL COMMENT '类型: 1.遍地红包 2.提现明细 3.',
  `targetid` int(11) unsigned NOT NULL COMMENT '目标对象: 1.微信 2.QQ 3.朋友圈 4.QQ空间 5.微博',
  `create_time` int(11) NOT NULL,
  `create_day` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '分享状态: 1.成功 2.失败 3.取消',
  `delete_id` int(11) NOT NULL DEFAULT '0' COMMENT '删除者id：0.正常  >0删除人',
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_share_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_share_type`;
CREATE TABLE `rm_user_share_type` (
  `id` int(11) NOT NULL,
  `typename` varchar(80) NOT NULL COMMENT '分享项目名称',
  `reward_way` tinyint(1) NOT NULL DEFAULT '0' COMMENT '奖励方式:0.未知 1.步币 2.金钱 3.步币+金钱',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `credit` int(11) NOT NULL DEFAULT '0',
  `delete_id` int(11) NOT NULL DEFAULT '0',
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_step
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_step`;
CREATE TABLE `rm_user_step` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL COMMENT '用户ID',
  `step_number` int(10) unsigned NOT NULL COMMENT '步数',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '同步时间',
  `devices_num` int(11) NOT NULL COMMENT '设备号',
  `create_day` date NOT NULL DEFAULT '0000-00-00' COMMENT '创建时间（日期）',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `updatetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '同步时间',
  PRIMARY KEY (`id`),
  KEY `create_day` (`create_day`) USING BTREE,
  KEY `userid` (`userid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=355701 DEFAULT CHARSET=utf8 COMMENT='用户步数表';

-- ----------------------------
-- Table structure for rm_user_token
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_token`;
CREATE TABLE `rm_user_token` (
  `token` varchar(50) NOT NULL COMMENT 'Token',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `expiretime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间',
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员Token表';

-- ----------------------------
-- Table structure for rm_user_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_type`;
CREATE TABLE `rm_user_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL COMMENT '类型名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户类型表';

-- ----------------------------
-- Table structure for rm_user_vip
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_vip`;
CREATE TABLE `rm_user_vip` (
  `cv_id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned DEFAULT '0' COMMENT '是否给自己：0否|1是',
  `vip_no` char(20) NOT NULL DEFAULT '' COMMENT '会员充值编号',
  `addid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值的人',
  `spend` decimal(9,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '会员花费',
  `apple` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '苹果内购价格',
  `day` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '天数',
  `create_day` date NOT NULL COMMENT '创建时间（日期）',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '同步时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `rback` tinyint(1) unsigned DEFAULT '0' COMMENT '回退标志：0未回退，1已回退',
  PRIMARY KEY (`cv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1255 DEFAULT CHARSET=utf8 COMMENT='会员记录表';

-- ----------------------------
-- Table structure for rm_user_vipcus
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_vipcus`;
CREATE TABLE `rm_user_vipcus` (
  `cuso_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned DEFAULT '0' COMMENT '是否给自己充，充值回调时使用：0否|1是',
  `cvip_no` char(20) NOT NULL DEFAULT '' COMMENT '商户会员充值编号',
  `addid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值的人',
  `spend` decimal(9,2) unsigned DEFAULT '0.00' COMMENT '花费',
  `apple` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '苹果内购价格',
  `day` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '天数',
  `create_day` date NOT NULL COMMENT '创建时间（日期）',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `rback` tinyint(1) unsigned DEFAULT '0' COMMENT '回退标志：0未回退，1已回退',
  PRIMARY KEY (`cuso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=964 DEFAULT CHARSET=utf8 COMMENT='会员记录表';

-- ----------------------------
-- Table structure for rm_user_vipcus_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_vipcus_record`;
CREATE TABLE `rm_user_vipcus_record` (
  `cvip_id` int(10) NOT NULL AUTO_INCREMENT,
  `cvip_no` char(20) NOT NULL DEFAULT '' COMMENT '会员充值编号',
  `userid` int(10) NOT NULL COMMENT '用户ID',
  `spend` decimal(9,2) unsigned DEFAULT '0.00' COMMENT '会员花费',
  `apple` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '苹果内购价格',
  `create_day` date NOT NULL COMMENT '创建时间（日期）',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '同步时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `rback` tinyint(1) unsigned DEFAULT '0' COMMENT '回退标志：0未回退，1已回退',
  PRIMARY KEY (`cvip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1139 DEFAULT CHARSET=utf8 COMMENT='会员记录表';

-- ----------------------------
-- Table structure for rm_user_vipgold
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_vipgold`;
CREATE TABLE `rm_user_vipgold` (
  `gvipid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值的人',
  `gvipno` char(20) NOT NULL DEFAULT '' COMMENT '商户会员充值编号',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否给自己充：0否|1是',
  `spend` decimal(9,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '花费',
  `apple` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '苹果内购价格',
  `day` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '天数',
  `create_day` date NOT NULL COMMENT '创建时间（日期）',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `ok` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `rback` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '回退标志：0未回退，1已回退',
  PRIMARY KEY (`gvipid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金牌商户会员记录表';

-- ----------------------------
-- Table structure for rm_user_vipgold_pre
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_vipgold_pre`;
CREATE TABLE `rm_user_vipgold_pre` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值的人',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='预约金牌商户会员记录表';

-- ----------------------------
-- Table structure for rm_user_vip_merge
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_vip_merge`;
CREATE TABLE `rm_user_vip_merge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `no` varchar(100) NOT NULL,
  `userid` int(11) unsigned NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '种类: 0.普通会员(商家会员) 1.金牌会员 2.金钻会员 3.黑钻会员',
  `speed_money` decimal(10,2) unsigned NOT NULL COMMENT '用户充值金额',
  `actual_money` decimal(10,2) unsigned NOT NULL COMMENT '实际付款到账的金额',
  `day` int(11) unsigned NOT NULL COMMENT '有限期',
  `start` int(11) unsigned NOT NULL COMMENT '会员有效起始时间',
  `end` int(11) NOT NULL COMMENT '会员到期时间',
  `create_time` int(11) NOT NULL,
  `create_day` date NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ok` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否支付: 0.未支付 1.已支付',
  `ok_time` int(11) unsigned NOT NULL,
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `rback` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否过期: 0.在有效期内 1.过期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_user_vip_record
-- ----------------------------
DROP TABLE IF EXISTS `rm_user_vip_record`;
CREATE TABLE `rm_user_vip_record` (
  `vip_id` int(10) NOT NULL AUTO_INCREMENT,
  `vip_no` char(20) NOT NULL DEFAULT '' COMMENT '会员充值编号',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `spend` decimal(9,2) unsigned DEFAULT '0.00' COMMENT '会员花费',
  `apple` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '苹果内购价格',
  `create_day` date NOT NULL COMMENT '创建时间（日期）',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '同步时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  `ok` tinyint(1) unsigned DEFAULT '0' COMMENT '确认支付: 0未支付 1支付成功',
  `ok_time` int(10) unsigned NOT NULL COMMENT '确认支付时间',
  `rback` tinyint(1) unsigned DEFAULT '0' COMMENT '回退标志：0未回退，1已回退',
  PRIMARY KEY (`vip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1495 DEFAULT CHARSET=utf8 COMMENT='会员记录表';

-- ----------------------------
-- Table structure for rm_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `rm_withdraw`;
CREATE TABLE `rm_withdraw` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(36) NOT NULL COMMENT '提现编号',
  `userid` int(10) NOT NULL COMMENT '用户ID',
  `typeid` int(10) NOT NULL COMMENT '账户类型',
  `amount` decimal(10,2) NOT NULL COMMENT '提现金额（要扣取1%+1的手续费）',
  `actual_amount` decimal(10,2) unsigned NOT NULL COMMENT '实际到账金额',
  `feemoney` decimal(10,2) unsigned NOT NULL COMMENT '手续费金额',
  `transaction_no` varchar(36) NOT NULL COMMENT '提现流水号',
  `status` int(10) NOT NULL COMMENT '提现状态 0提现审核中 1提现成功 2提现失败',
  `create_time` int(10) NOT NULL COMMENT '提现时间',
  `remark` varchar(200) NOT NULL COMMENT '打款备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=798 DEFAULT CHARSET=utf8 COMMENT='用户提现表';

-- ----------------------------
-- Table structure for rm_withdraw_check
-- ----------------------------
DROP TABLE IF EXISTS `rm_withdraw_check`;
CREATE TABLE `rm_withdraw_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdraw_id` int(11) NOT NULL COMMENT '预提现记录id',
  `withdraw_wx_id` int(11) NOT NULL COMMENT '微信预提现id',
  `typeid` int(10) NOT NULL COMMENT '提现类型id',
  `admin_id` int(10) NOT NULL COMMENT '管理员id',
  `userid` int(10) NOT NULL COMMENT '用户id',
  `check_status` int(2) NOT NULL COMMENT '审核结果: 1.通过 2.拒绝',
  `reason_id` int(10) NOT NULL COMMENT '不通过原因',
  `remark` varchar(160) NOT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL,
  `delete_id` int(10) NOT NULL DEFAULT '0',
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `withdraw_id` (`withdraw_id`) USING BTREE,
  KEY `userid` (`userid`) USING BTREE,
  KEY `check_status` (`check_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='预提现审核记录表';

-- ----------------------------
-- Table structure for rm_withdraw_check_reason
-- ----------------------------
DROP TABLE IF EXISTS `rm_withdraw_check_reason`;
CREATE TABLE `rm_withdraw_check_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(180) NOT NULL COMMENT '审核理由',
  `delete_id` int(10) NOT NULL DEFAULT '0',
  `delete_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='提现审核缘由';

-- ----------------------------
-- Table structure for rm_withdraw_log
-- ----------------------------
DROP TABLE IF EXISTS `rm_withdraw_log`;
CREATE TABLE `rm_withdraw_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户提现账号id',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `order_no` varchar(36) NOT NULL COMMENT '提现id',
  `typeid` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '提现类型： 1微信 2支付宝 3银行卡',
  `style` tinyint(1) NOT NULL DEFAULT '1' COMMENT '记录类型:1预提现 2提现到账',
  `realname` varchar(20) NOT NULL COMMENT '真实姓名',
  `bank_card` varchar(120) NOT NULL COMMENT '银行卡',
  `opening_bank` varchar(255) NOT NULL COMMENT '开户行',
  `subbranch` varchar(255) NOT NULL COMMENT '开户支行',
  `region` varchar(255) NOT NULL COMMENT '开户地区',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '状态: 1.已到账   0.未到账',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2973 DEFAULT CHARSET=utf8 COMMENT='用户银行卡表';

-- ----------------------------
-- Table structure for rm_withdraw_pre
-- ----------------------------
DROP TABLE IF EXISTS `rm_withdraw_pre`;
CREATE TABLE `rm_withdraw_pre` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '预提现id',
  `userid` int(10) NOT NULL COMMENT '用户id',
  `order_no` varchar(200) NOT NULL COMMENT '订单号',
  `typeid` int(10) NOT NULL COMMENT '提现类型',
  `amount` decimal(10,2) NOT NULL COMMENT '金额',
  `actual_amount` decimal(10,2) unsigned NOT NULL COMMENT '提现金额（要扣取1%+1的手续费）',
  `feemoney` decimal(10,2) unsigned NOT NULL COMMENT '手续费金额',
  `wx_openid` varchar(180) NOT NULL COMMENT '微信openid',
  `wx_unionid` varchar(160) NOT NULL COMMENT '微信unionid',
  `nickname` varchar(70) NOT NULL COMMENT '微信昵称',
  `sex` tinyint(1) NOT NULL COMMENT '性别: 0.女 1.男',
  `avatar` varchar(220) NOT NULL COMMENT '微信头像',
  `province` varchar(50) NOT NULL COMMENT '微信省份',
  `city` varchar(50) NOT NULL COMMENT '微信城市',
  `area` varchar(50) NOT NULL COMMENT '微信区域',
  `bank_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '银行卡id',
  `account_name` varchar(50) NOT NULL COMMENT '银行账户',
  `bank_code` varchar(50) NOT NULL COMMENT '银行编码',
  `bank_card` varchar(50) NOT NULL COMMENT '银行卡',
  `bank_phone` varchar(20) NOT NULL COMMENT '银行卡',
  `status` tinyint(1) NOT NULL COMMENT '预提现状态: 0未处理，1提现成功，2提现失败，3提现出错',
  `check_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '审核状态: 0.未审 1.通过 2.拒绝',
  `delete_id` int(11) NOT NULL DEFAULT '0' COMMENT '删除标识: 0.未删除  >0.删除者id',
  `delete_time` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(160) NOT NULL COMMENT '备注',
  `tno` varchar(200) NOT NULL COMMENT '交易号（合利宝流水号，微信流水号等）',
  `statime` int(11) NOT NULL DEFAULT '0' COMMENT '状态修改时间',
  `check_time` int(11) NOT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1516 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rm_withdraw_type
-- ----------------------------
DROP TABLE IF EXISTS `rm_withdraw_type`;
CREATE TABLE `rm_withdraw_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '账户名称',
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='账户类型表';

-- ----------------------------
-- Table structure for rm_withdraw_wx
-- ----------------------------
DROP TABLE IF EXISTS `rm_withdraw_wx`;
CREATE TABLE `rm_withdraw_wx` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '微信提现id',
  `order_no` varchar(60) NOT NULL COMMENT '提现编号',
  `typeid` int(2) NOT NULL COMMENT '提现类型: 1.微信 2.支付宝 3.银行卡',
  `wx_openid` varchar(200) NOT NULL COMMENT '微信openid',
  `wx_unionid` varchar(200) NOT NULL COMMENT '微信unionid',
  `userid` int(11) NOT NULL COMMENT '用户唯一id',
  `money` decimal(11,2) NOT NULL COMMENT '提现金额',
  `actual_money` decimal(11,2) NOT NULL COMMENT '用户实际到账金额',
  `fee_money` decimal(11,2) NOT NULL COMMENT '平台提现手续费',
  `nickname` varchar(100) NOT NULL COMMENT '微信昵称',
  `sex` tinyint(1) NOT NULL COMMENT '性别:  0.女 1.男',
  `avatar` varchar(200) NOT NULL COMMENT '微信提现授权的用户头像',
  `province` varchar(200) NOT NULL COMMENT '微信授权省份',
  `city` varchar(200) NOT NULL COMMENT '微信授权所在城市名',
  `area` varchar(200) NOT NULL COMMENT '微信授权所在区域',
  `status` int(2) NOT NULL COMMENT '提现状态:0.处理中 1.成功 2.失败',
  `create_time` int(11) NOT NULL COMMENT '用户提现申请时间',
  `check_status` int(2) NOT NULL COMMENT '后台审核状态: 0.未审核 1.通过 2.拒绝',
  `check_time` int(11) NOT NULL COMMENT '审核时间',
  `status_time` int(11) NOT NULL COMMENT '提现状态时间: 到账时间, 提现出错时间',
  `delete_id` int(2) NOT NULL DEFAULT '0',
  `delete_time` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='微信提现记录表';

-- ----------------------------
-- Table structure for rm_xcx_goods
-- ----------------------------
DROP TABLE IF EXISTS `rm_xcx_goods`;
CREATE TABLE `rm_xcx_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '商品名称',
  `old_price` decimal(10,2) NOT NULL COMMENT '原价',
  `stock` int(11) NOT NULL COMMENT '库存',
  `exchange_type` varchar(30) NOT NULL COMMENT '兑换类型: 1.步币，2.现金 1,2.步币+现金',
  `step_credit` int(11) NOT NULL,
  `cash` decimal(10,2) NOT NULL,
  `is_mail` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否包邮: 0.否 1.是',
  `mail_money` decimal(10,2) NOT NULL COMMENT '邮寄费',
  `goods_intro` varchar(255) NOT NULL COMMENT '商品介绍',
  `create_time` int(11) NOT NULL,
  `create_admin` int(11) NOT NULL,
  `is_entity` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否为实物: 0.否 1.是',
  `is_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶: 0.否 1.是',
  `status` tinyint(1) NOT NULL COMMENT '状态: 0.上架  1.下架',
  `last_up_time` int(11) NOT NULL COMMENT '上架时间',
  `last_down_time` int(11) NOT NULL COMMENT '下架时间',
  `delete_id` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='小程序兑换商品';

-- ----------------------------
-- Table structure for rm_xcx_share
-- ----------------------------
DROP TABLE IF EXISTS `rm_xcx_share`;
CREATE TABLE `rm_xcx_share` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '小程序分享页面数据id',
  `title` char(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(200) NOT NULL COMMENT '内容',
  `img` varchar(500) NOT NULL COMMENT '图片路径',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `delete_id` int(10) unsigned DEFAULT '0' COMMENT '删除者id',
  `delete_time` int(10) unsigned NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for rm_ztest
-- ----------------------------
DROP TABLE IF EXISTS `rm_ztest`;
CREATE TABLE `rm_ztest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content` text NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=947 DEFAULT CHARSET=utf8 COMMENT='账户类型表';
