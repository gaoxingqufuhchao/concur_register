<?php
namespace app\index\job;
use think\queue\Job;
use think\Db;
use think\Exception;
use think\facade\Cache;
use think\facade\Env;

class JobUser {

    private  $cache;
    private  $handler;
    public  function  __construct()
    {
        $this->cache = Cache::init();
        $this->handler = $this->cache->handler();
    }

    /**
     * fire方法是消息队列默认调用的方法
     * @param Job            $job      当前的任务对象
     * @param array|mixed    $data     发布任务时自定义的数据
     */
    public function fire(Job $job,$data) {
        $job->delete();
        //print("hahah\n");
        // print("<info>The user already exists "."</info>\n");
        //     exit();
        if(empty($data) || empty($data['userid']) || empty($data['mobile'])) {
            $job->delete();
            print("canshu buzu\n");
            return;
        }

        // 如有必要,可以根据业务需求和数据库中的最新数据,判断该任务是否仍有必要执行.
        $isJobStillNeedToBeDone = $this->checkDatabaseToSeeIfJobNeedToBeDone($data);
        if(!$isJobStillNeedToBeDone) {
            print("hahah\n");
            $job->delete();
            return;
        }

        $isJobDone = $this->doHelloJob($data);

        if ($isJobDone) {
            //如果任务执行成功， 记得删除任务
            $job->delete();
            print("<info>Hello Job has been done and deleted"."</info>\n");
        }else{
            if ($job->attempts() > 3) {
                //通过这个方法可以检查这个任务已经重试了几次了
                print("<warn>Hello Job has been retried more than 3 times!"."</warn>\n");
                //$job->delete();
                // 也可以重新发布这个任务
                //print("<info>Hello Job will be availabe again after 2s."."</info>\n");
                //$job->release(2); //$delay为延迟时间，表示该任务延迟2秒后再执行
            }
        }
    }

    /**
     * 有些消息在到达消费者时,可能已经不再需要执行了
     * @param array|mixed    $data     发布任务时自定义的数据
     * @return boolean                 任务执行的结果
     */
    private function checkDatabaseToSeeIfJobNeedToBeDone($data) {
        // 判断手机缓存集合中是否存在
        // $is_existe = $this->handler->sIsMember("register:mobile",$data['mobile']);
        // if($is_existe) {
        //     return false;  
        // } 

        // // 查询当前用户是否在数据库中存在
        // $userinfo = Db::name('user')->field('id')->where('id',$data['userid'])->find();
        // if($userinfo) {
        //     return false;  
        // } 

        return true;
    }

    /**
     * 根据消息中的数据进行实际的业务处理
     * @param array|mixed    $data     发布任务时自定义的数据
     * @return boolean                 任务执行的结果
    */
    private function doHelloJob($data) {
        try{

            if(isset($data['inviterid']) && !empty($data['inviterid'])) {

                // 添加邀请记录
                $res_record = Db::name('user_inviter')
                    ->insert([
                        'inviterid'   => $data['inviterid'],
                        'userid'      => $data['userid'],
                        'code'        => $data['inviterid'] . 'T' . $data['userid'],
                        'create_time' => $data['time'],
                ]);

                // 给邀请人赠送300步币
                Db::name('user_credit')
                    ->insert([
                        'userid'      => $data['inviterid'],
                        'type'        => 1,
                        'credit'      => 300,
                        'source'      => $res_record,
                        'create_time' => $data['time']
                ]);

                // 更新邀请人步币（用户表）
                Db::name('user')->where('id', $data['inviterid'])->setInc('credit', 300);              
            }
            
            {   // 注册成功发表动态
                $dynamic_data['userid'] = $data['userid'];
                $dynamic_data['dynamic'] = base64_encode('号外！号外！我加入跑步钱进了，大家一起走路领红包吧！');
                $dynamic_data['images'][] = 'https://rumcdn-1255484416.cos.ap-chengdu.myqcloud.com/img/d_d.png';
                $dynamic_data['images'] = serialize($dynamic_data['images']);
                $dynamic_data['create_time'] = $data['time'];

                $result = Db::name('dynamic')->insert($dynamic_data);
            }

        }catch(\Exception $e) {
            Log::write('---执行消息队列出错---'.$e->getMessage());
            return false;
        }

        return true;

        // 根据消息中的数据进行实际的业务处理...
        //var_dump($data);
//        print("<info>Hello Job Started. job Data is: ".var_export($data,true)."</info> \n");
//        print("<info>Hello Job is Fired at " . date('Y-m-d H:i:s') ."</info> \n");
//        print("<info>Hello Job is Done!"."</info> \n");

        //return true;
    }

    /**
     * 该方法用于接收任务执行失败的通知，你可以发送邮件给相应的负责人员
     * @param $jobData  string|array|...      //发布任务时传递的 jobData 数据
    */
    public function failed($jobData) {
        //send_mail_to_somebody() ;
        print("Warning: Job failed after max retries. job data is :".var_export($jobData,true)."\n");
    }

}